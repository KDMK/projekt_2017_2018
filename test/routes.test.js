process.env.NODE_ENV = 'test';

const chai = require('chai');
const request = require('request');
const should = chai.should();
const expect = chai.expect;
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const server = require('../app/app');

// TODO: Find out how to write test for DAO layer
describe('routes : auth', () => {

  describe('DATABASE', () => {
    it('Should be up', () => {
      request('http://localhost:5000/api/database', (error, response, body) => {
        expect(response.statusCode).to.equal(200);
      });
    });
  });

});