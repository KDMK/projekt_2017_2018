INSERT INTO project.category (id, parent, created_by, edited_by) VALUES (1, null, 1, 1);
INSERT INTO project.category (id, parent, created_by, edited_by) VALUES (2, null, 1, 1);
INSERT INTO project.category (id, parent, created_by, edited_by) VALUES (3, null, 1, 1);
INSERT INTO project.category (id, parent, created_by, edited_by) VALUES (4, null, 1, 1);
INSERT INTO project.category (id, parent, created_by, edited_by) VALUES (5, null, 1, 1);
INSERT INTO project.category (id, parent, created_by, edited_by) VALUES (6, null, 1, 1);
INSERT INTO project.category (id, parent, created_by, edited_by) VALUES (7, null, 1, 1);
INSERT INTO project.category (id, parent, created_by, edited_by) VALUES (8, null, 1, 1);
INSERT INTO project.category (id, parent, created_by, edited_by) VALUES (9, null, 1, 1);


INSERT INTO project.category_localized_data (category_id, language_id, name) VALUES(1, 1, 'Restaurants');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(1, 2, 'Restorani');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(1, 3, '음식점');

INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(2, 1, 'Bars');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(2, 2, 'Barovi');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(2, 3, '술집');

INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(3, 1, 'Small and quick bites');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(3, 2, 'Mali i brzi zalogaji');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(3, 3, '간식');

INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(4, 1, 'Fun culture spots');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(4, 2, 'Zabavna mjesta kulture');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(4, 3, '오락 ');

INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(5, 1, 'Out of Zagreb');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(5, 2, 'Izvan Zagreba');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(5, 3, '자그레브 외곽');

INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(6, 1, 'Paistry shops');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(6, 2, 'Slastičarnice');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(6, 3, '제과점');

INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(7, 1, 'Events');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(7, 2, 'Događanja');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(7, 3, '이벤트');

INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(8, 1, 'Cafes');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(8, 2, 'Kafići');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(8, 3, '카페');

INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(9, 1, 'Night clubs');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(9, 2, 'Noćni klubovi');
INSERT INTO project.category_localized_data(category_id, language_id, name) VALUES(9, 3, '나이트클럽');