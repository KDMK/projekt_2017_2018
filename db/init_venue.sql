INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(1, 'https://wwww.otto-franck.com', 'Tkalciceva 20', '0038514824288','otto.frank.bar@gmail.com', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(1, 1, 'Otto&Frank', 'Cafe in the liveliest pedestrian street featuring a local favourite Zagreb breakfast');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(1, 2, 'Otto&Frank', 'Kafić u najživljoj pješačkoj ulici s omiljenim, lokalnim zagrebačkim doručkom');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(1, 3, 'Otto&Frank', '통행자가 많은 거리에 위치한 카페. 자그레브 시민들이 가장 좋아하는 아침식사 ');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(1, 3);
INSERT INTO project.category_venue(venue_id, category_id) VALUES(1, 2);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(2, 'https://www.plac-zagreb.com', 'Dolac 2', '0038514876761','info@plac-zagreb.com', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(2, 1, 'Plac', 'The best grill cevapi (minced kebab - local favourite)');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(2, 2, 'Plac', 'Najbolji ćevapi');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(2, 3, 'Plac', '최고의 그릴 cevapi(현지인이 좋아하는 케밥)');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(2, 3);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(3, null, 'Ulica Lavoslava Ruzicke 50', '0038515516000', null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(3, 1, 'Burek', 'The old school burek (cheese leafy pie) in very traditional settings');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(3, 2, 'Burek', 'Burek u vrlo tradicionalnom okruženju');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(3, 3, 'Burek', '옛날 분위기에서 전통적인 burek(치즈 듬뿍 파이)');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(3, 3);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(4, 'https://submarineburger.com', 'Frankopanska 11', '0038514831500','info@submarineburger.com', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(4, 1, 'Submarine', 'The best burgers in town');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(4, 2, 'Submarine', 'Najbolji hamburgeri u gradu');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(4, 3, 'Submarine', '시내에서 최상의 버거');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(4, 3);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(5, null, 'Skalinska 5', '0038514837701', null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(5, 1, 'La Štruk', 'Indulge in local favourite pastry made of dough and cheese, with a modern twist');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(5, 2, 'La Štruk', 'Razmazite se lokalno omiljenom slasticom od tijesta i sira, s modernim obratom');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(5, 3, 'La Štruk', '현대적 감각이 가미된 도우와 치즈로 만든 로컬들이 가장 좋아하는 제과점');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(5, 3);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(6, 'http://prizvoncu.com/', 'XII Vrbik', '0038516198473', null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(6, 1, 'Pri zvoncu', 'Great food, the best steak and strukli in traditional settings');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(6, 2, 'Pri zvoncu', 'Odlična hrana, najbolji odrezak i štrukli u tradicionalnom okruženju');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(6, 3, 'Pri zvoncu', '훌륭한 음식, 전통적 분위기에 최상의 스테이크');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(6, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(7, 'http://www.evergreensushi.com/', 'Ilica 49', '00385958800800', null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(7, 1, 'Evergreen sushi', 'Best affordable sushi in town in a art deco building');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(7, 2, 'Evergreen sushi', 'Najbolji sushi u gradu po pristupačnoj cijeni u art deco zgradi');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(7, 3, 'Evergreen sushi', '예술적 인테리어 건물에 시내에서 가장 가성비 좋은 스시');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(7, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(8, 'http://www.restoran.nokturno.hr/', 'Skalinska 4', '0038514813394', 'rezervacije@nokturno.hr', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(8, 1, 'Nokturno', 'Quick cheap bites');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(8, 2, 'Nokturno', 'Brzo gotova, povoljna hrana u centru grada');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(8, 3, 'Nokturno', '신속 저렴한 간식');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(8, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(9, null, 'Petrinjska 2', '0038517888777', null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(9, 1, 'Mundoaka', 'Centrally located, great pork ribs');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(9, 2, 'Mundoaka', 'Restoran u centru grada, odlična svinjska rebarca');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(9, 3, 'Mundoaka', '중심부 위치, 훌륭한 돼지갈비');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(9, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(10, 'http://rougemarin.hr/', 'Ulica Frane Folnegovica 10', '0038516187776', 'info@rougemarin.hr', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(10, 1, 'Ružmarin', 'Creative dishes in a beautiful ex-light bulb factory garden');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(10, 2, 'Ružmarin', 'Kreativna hrana u lijepoj, nekadašnjoj tvornici žarulja');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(10, 3, 'Ružmarin', '전구공장의 가든을 개조. 창의적 음식');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(10, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(11, 'https://www.konoba-didovsan.com/', 'Mletacka 11', '0038514851154', 'gornji-grad@konoba-didovsan.com', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(11, 1, 'Didov san', 'A traditional restaurant in beautiful old town settings');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(11, 2, 'Didov san', 'Tradicionalni restoran u lijepom starogradskom okruženju');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(11, 3, 'Didov san', '아름다운 시골 분위기의 전통적 식당');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(11, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(12, 'http://thegarden.hr/brewery/', 'Slavonska Avenija 22F', null, 'info@thegarden.hr', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(12, 1, 'The Garden Brewery', 'Local artisan craft brewery and burger bar located in a former factory in an urban industrial setting');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(12, 2, 'The Garden Brewery', 'Lokalna obrtnička pivovara i burger bar u nekadašnjoj tvornici u urbanom industrijskom okruženju');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(12, 3, 'The Garden Brewery', '과거 공장을 현대적 인더스트리얼 분위기로 개조. 로컬에서 제조한 맥주와 햄버거');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(12, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(13, 'http://duksa.hr/', 'Duknoviceva 4', '0038512334556', 'duksa.pizza@gmail.com', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(13, 1, 'Duksa Pizza', 'One of the best thin crust pizzas in town');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(13, 2, 'Duksa Pizza', 'Jedna od najboljih pizza tankog tijesta u gradu ');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(13, 3, 'Duksa Pizza', '시내에서 최고인 얇은 크러스트 피자중 하나');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(13, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(14, 'http://www.laripenati.hr/', 'Petrinjska 42a', '0038514655776', 'info@laripenati.hr', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(14, 1, 'Lari&Penati', 'Modern bistro offerring affordable daily changing menus');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(14, 2, 'Lari&Penati', 'Moderni bistro koji nudi dnevne menije po pristupačnim cijenama');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(14, 3, 'Lari&Penati', '매일매일 적당한 가격의 메뉴로 바뀌는 모던한 비스트로 ');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(14, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(15, 'http://www.dubravkin-put.com/', 'Dubravkin put 2', '0038514834975', 'info@dubravkin-put.com', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(15, 1, 'Dubravkin Put', 'A classy fine dining restaurant located at the entrance into the Tuškanac forest');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(15, 2, 'Dubravkin Put', 'Otmjen restoran na ulazu u šumu Tuškanac');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(15, 3, 'Dubravkin Put', 'Tuškanac forest 입구에 위치한 클랙식한 분위기의 파인 다이닝 음식점 ');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(15, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(16, 'http://www.pivovara-medvedgrad.hr/mali-medo/', 'Tkalciceva 36', '0038514929613', null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(16, 1, 'Mali Medo', 'Great selection of home brewed beers in the the liveliest pedestrian street');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(16, 2, 'Mali Medo', 'Odličan izbor domaćih piva u najživljoj pješačkoj ulici');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(16, 3, 'Mali Medo', '통행자가 많은 거리에 위치. 다양한 수제 맥주');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(16, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(17, 'http://magazinskaklet-bistro.com.hr/', 'Magazinska ulica 7', '0038513091717', 'magazinskaklet.bistro@gmail.com', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(17, 1, 'Magazinska Klet', 'Arguably the best local barbecue in town in old style setting');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(17, 2, 'Magazinska Klet', 'Nedvojbeno najbolji lokalni roštilj u gradu u starogradskom okruženju');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(17, 3, 'Magazinska Klet', '전통적 분위기에 아마도 시내에서 최고의 바베큐');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(17, 1);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(18, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(18, 1, 'Vincek', 'An old school style paistry shop');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(18, 2, 'Vincek', 'Tradicionalna slastičarnica');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(18, 3, 'Vincek', '전통적 스타일의 제과점');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(18, 6);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(19, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(19, 1, 'Time', 'Designer cakes inspired by grandmas recepies');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(19, 2, 'Time', 'Ručno izrađen kolač inspiriran bakinim receptima');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(19, 3, 'Time', '할머니의 조리법에 착안한 디자이너의 케이크');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(19, 6);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(20, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(20, 1, 'Torte i to', 'Cheese and chocolate cakes to die for');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(20, 2, 'Torte i to', 'Odlični čokoladni kolači i kolači od sira');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(20, 3, 'Torte i to', '정말로 맛있는 치즈 초콜렛 케이크');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(20, 6);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(21, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(21, 1, 'Chocolat 041', 'The best chocolate ice cream in town in hip settings');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(21, 2, 'Chocolat 041', 'Najbolji čokoladni sladoled u gradu u modernom okruženju');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(21, 3, 'Chocolat 041', '힙한 분위기에 최고의 초코렛 아이스크림');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(21, 6);



INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(22, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(22, 1, 'Vostinic-Klasnic', 'A young couple leaves office jobs takes over grandpas wineyard to make amazin wine');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(22, 2, 'Vostinic-Klasnic', 'Mladi par napušta uredske poslove i preuzima djedov vinograd kako bi radio odlična vina');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(22, 3, 'Vostinic-Klasnic', '젋은 커플들이 사무실에서 벗어나 와인농장에서 와인 만들기 체험 ');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(22, 5);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(23, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(23, 1, 'Tesla', 'Hip crowd in a central location.');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(23, 2, 'Tesla', 'Provod uz suvremeno društvo u centru grada');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(23, 3, 'Tesla', '중심부에 위치한 힙한 곳');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(23, 9);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(24, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(24, 1, 'Opera Club', 'Dancing to the EDM till morning');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(24, 2, 'Opera Club', 'Plesanje na elektronsku glazbu do jutra');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(24, 3, 'Opera Club', '아침까지 EDM(전자 댄스 뮤직)에 맞춰 춤을');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(24, 9);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(25, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(25, 1, 'Pod starim krovovima', 'An old-school smoke-filled pub, go for atmoshpere');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(25, 2, 'Pod starim krovovima', 'Starinski, dimom ispunjena kavana, posjetite zbog atmosfere');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(25, 3, 'Pod starim krovovima', '전통적, 연기가 자욱한 술집. 분위기로 갈만한 곳');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(25, 4);
INSERT INTO project.category_venue(venue_id, category_id) VALUES(25, 2);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(26, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(26, 1, 'Kino Europa', 'Beautiful classical cinema with a great progame');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(26, 2, 'Kino Europa', 'Lijepo, klasično kino s odličnim programom');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(26, 3, 'Kino Europa', '좋은 프로그램을 가진 아름다운 전통적 영화관');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(26, 4);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(27, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(27, 1, 'Ljetno Kino Tuskanac', 'Watch a movie under the stars while sipping a cocktail');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(27, 2, 'Ljetno Kino Tuskanac', 'Pogledajte film pod zvijezdama dok pijuckate koktel');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(27, 3, 'Ljetno Kino Tuskanac', '칵테일을 마시면서 별아래서 영화 감상');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(27, 4);



INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(28, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(28, 1, 'Dvorišta', 'Backyards turned into bars once per year');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(28, 2, 'Dvorišta', 'Stara dvorišta postaju barovi jednom godišnje');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(28, 3, 'Dvorišta', '일년에 한 번 술집으로 변하는 정원');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(28, 7);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(29, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(29, 1, 'Velvet Café', 'Arguably the most most beatuful cafe in Zagreb. The first cafe in the world that has been turned into a free reading zone');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(29, 2, 'Velvet Café', 'Nedvojbeno najljepši kafić u Zagrebu. Prvi kafić u svijetu koji je pretvoren u zonu besplatnog čitanja');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(29, 3, 'Velvet Café', '아마도 자그레브에서 가장 아름다운 카페. 무료로 독서가 가능한 영역을 만든 세계 최초의 카페');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(29, 8);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(30, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(30, 1, 'Kava tava', 'Quirky 50s feel cozy decor, american style pancakes, coffee and great breakfasts/brunch');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(30, 2, 'Kava tava', 'Osebujan i udoban dekor u stilu pedesetih, američke palačinke, kava i odličan doručak/užina ');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(30, 3, 'Kava tava', '50년대 분위기의 편안한 데코, 아메리칸 스타일 팬케이크, 커피, 훌륭한 아침/브런치');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(30, 6);
INSERT INTO project.category_venue(venue_id, category_id) VALUES(30, 8);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(31, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(31, 1, 'Kino Europa Cafe', 'Cafe located in a beautiful classical cinema where patrons sit in black directors chairs');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(31, 2, 'Kino Europa Cafe', 'Kafić u lijepom klasičnom kinu gdje posjetitelji sjede u crnim, direktorskim stolicama');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(31, 3, 'Kino Europa Cafe', '고객들이 검정색 영화감독의 의자에 앉을 수 있는 카페. 아름다운 전통적 영화관에 위치');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(31, 8);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(32, 'http://www.kraschocobar.com/en/', 'Ilica 15', '0038514876362', null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(32, 1, 'Choco bar Bonbonniere Kras', 'Choco bar selling delicious local sweets');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(32, 2, 'Choco bar Bonbonniere Kras', 'Čokoladni bar s ukusnim lokalnim poslasticama');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(32, 3, 'Choco bar Bonbonniere Kras', '맛있는 현지의 디저트를 파는 초코렛 바');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(32, 8);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(33, null, 'Ulica Augusta Cesarca 2', '00385952888628', null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(33, 1, 'Cheese bar', 'Fancy wine bar with good wine & cheese selection');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(33, 2, 'Cheese bar', 'Elegantni vinski bar s dobrim izborom vina i sireva');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(33, 3, 'Cheese bar', '좋은 와인과 치즈 셀렉션을 가진 분위기 좋은 와인바');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(33, 2);
INSERT INTO project.category_venue(venue_id, category_id) VALUES(33, 3);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(34, 'http://www.bornstein.hr/en/home/', 'Kaptol 19', '0038514812363', 'bornstein@bornstein.hr', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(34, 1, 'Wine Bar Bornstein', 'How about a glass of wine in a magnificient 200 y.o. brick vaulted wine cellar');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(34, 2, 'Wine Bar Bornstein', 'Što kažete na čašu vina u prekrasnom 200 godina starom vinskom podrumu sa ciglenim svodom');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(34, 3, 'Wine Bar Bornstein', '웅장한 규모의 200년된 아치형 벽돌로 둘러싸인 와인 셀러에서 와인 한잔은 어떤가요');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(34, 2);
INSERT INTO project.category_venue(venue_id, category_id) VALUES(34, 3);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(35, null, 'Horvacanska cesta 3', null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(35, 1, 'A most unusual garden', 'Alice in wonderland inspired bar/cafe. Grab a drink in the house tree');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(35, 2, 'A most unusual garden', 'Kafić inspiriran Alisom u zemlji čudesa. Popijte piće u kući na stablu');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(35, 3, 'A most unusual garden', '50년대 분위기의 편안한 데코, 아메리칸 스타일 팬케이크, 커피, 훌륭한 아침/브런치');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(35, 8);
INSERT INTO project.category_venue(venue_id, category_id) VALUES(35, 2);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(36, 'http://johannfranck.hr/', 'Trg bana Josipa Jelačića 9', '00385917838153', 'johann.franck@johannfranck.hr', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(36, 1, 'Johann Franck', 'Central square people watching turns into a night club on weekends');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(36, 2, 'Johann Franck', 'Kavana s pogledom na glavni trg koja se vikendom pretvara u noćni klub');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(36, 3, 'Johann Franck', '시내에 위치, 주말에는 나이트클럽으로 향하는 사람들의 행렬 볼 수 있음');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(36, 2);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(37, null, 'Pod Zidom 5', '00385993253600', null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(37, 1, 'Pod zidom', 'Modern wine bar with good wine selection');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(37, 2, 'Pod zidom', 'Suvremeni vinski bar s dobrim izborom vina');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(37, 3, 'Pod zidom', '훌륭한 와인 셀렉션을 가진 현대적 와인바');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(37, 2);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(38, 'http://www.dezman.hr/', 'Dežmanova 1', '0038514846160', 'info@dezman.hr', 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(38, 1, 'Dežman bar', 'A modern bar with an excellent choice of vine, great coctails and gourment small bites.');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(38, 2, 'Dežman bar', 'Suvremeni bar s odličnim izborom vina, koktela i gurmanskih zalogaja');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(38, 3, 'Dežman bar', '최상의 와인과 칵테일, 고멧 스타일 간식의 현대적 술집');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(38, 2);


INSERT INTO project.venue(id, website, address, phone, email, created_by, edited_by) VALUES(39, null, null, null, null, 1, 1);
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(39, 1, 'Preradoviceva street', 'Stroll through alterantive scene along with cheep drinks');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(39, 2, 'Preradoviceva street', 'Prošetajte kroz alternativnu scenu uz jeftino piće');
INSERT INTO project.venue_localized_data(venue_id, language_id, name, description) VALUES(39, 3, 'Preradoviceva street', '히피들이 모이는 저렴한 술집');
INSERT INTO project.category_venue(venue_id, category_id) VALUES(39, 2);