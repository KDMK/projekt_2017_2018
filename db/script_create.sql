CREATE SCHEMA project
  AUTHORIZATION projektadmin;

-- Create tables
CREATE TABLE project.language (
  id         BIGSERIAL PRIMARY KEY,
  name       VARCHAR(255) NOT NULL UNIQUE,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE project.role (
  id         BIGSERIAL PRIMARY KEY,
  name       VARCHAR(255) NOT NULL UNIQUE,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE project.users (
  id         BIGSERIAL PRIMARY KEY,
  role_id    BIGINT       NOT NULL,
  username   VARCHAR(255) NOT NULL UNIQUE,
  password   VARCHAR(255) NOT NULL,
  full_name  VARCHAR(255) NOT NULL,
  active     BOOLEAN DEFAULT TRUE,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  token      VARCHAR(255)
);

CREATE TABLE project.category (
  id         BIGSERIAL PRIMARY KEY,
  parent     BIGINT,
  created_by BIGINT NOT NULL,
  edited_by  BIGINT NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE project.category_localized_data (
  id          BIGSERIAL PRIMARY KEY,
  category_id BIGINT       NOT NULL,
  language_id BIGINT       NOT NULL,
  name        VARCHAR(255) NOT NULL UNIQUE,
  created_at  TIMESTAMP WITH TIME ZONE,
  updated_at  TIMESTAMP WITH TIME ZONE
);

CREATE TABLE project.venue (
  id          BIGSERIAL PRIMARY KEY,
  website     VARCHAR(255),
  address     VARCHAR(255),
  phone       VARCHAR(255),
  email       VARCHAR(255) UNIQUE,
  rating      DECIMAL DEFAUlT 0,
  created_by  BIGINT NOT NULL,
  edited_by   BIGINT NOT NULL,
  created_at  TIMESTAMP WITH TIME ZONE,
  updated_at  TIMESTAMP WITH TIME ZONE
);

CREATE TABLE project.venue_localized_data (
  id          BIGSERIAL PRIMARY KEY,
  venue_id    BIGINT       NOT NULL,
  language_id BIGINT       NOT NULL,
  name        VARCHAR(255) NOT NULL,
  description TEXT         NOT NULL,
  created_at  TIMESTAMP WITH TIME ZONE,
  updated_at  TIMESTAMP WITH TIME ZONE
);

CREATE TABLE project.review (
  id         BIGSERIAL PRIMARY KEY,
  venue_id   BIGINT,
  title      VARCHAR(255),
  content    TEXT,
  created_by BIGINT NOT NULL,
  edited_by  BIGINT NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE project.rating (
  id         BIGSERIAL PRIMARY KEY,
  venue_id   BIGINT   NOT NULL,
  rating     SMALLINT NOT NULL,
  created_by BIGINT   NOT NULL,
  edited_by BIGINT   NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE project.category_venue (
  id           BIGSERIAL PRIMARY KEY,
  category_id  BIGINT,
  venue_id     BIGINT,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

-- Create constraints keys
ALTER TABLE project.users
  ADD CONSTRAINT fk_role_user
FOREIGN KEY (role_id) REFERENCES project.role (id);

ALTER TABLE project.category
  ADD CONSTRAINT fk_user_category
  FOREIGN KEY (created_by) REFERENCES project.users (id);
ALTER TABLE project.category
  ADD CONSTRAINT fk_parent_category
  FOREIGN KEY (parent) REFERENCES project.category (id);

ALTER TABLE project.category_localized_data
  ADD CONSTRAINT fk_category_id_category
  FOREIGN KEY (category_id) REFERENCES project.category (id) ON DELETE CASCADE;
ALTER TABLE project.category_localized_data
  ADD CONSTRAINT fk_language_id_language
  FOREIGN KEY (language_id) REFERENCES project.language (id);

ALTER TABLE project.venue
  ADD CONSTRAINT fk_created_by_venue
  FOREIGN KEY (created_by) REFERENCES project.users (id);
ALTER TABLE project.venue
  ADD CONSTRAINT fk_edited_by_venue
  FOREIGN KEY (edited_by) REFERENCES project.users (id);

ALTER TABLE project.venue_localized_data
  ADD CONSTRAINT fk_language_venue_name
  FOREIGN KEY (language_id) REFERENCES project.language (id);
ALTER TABLE project.venue_localized_data
  ADD CONSTRAINT fk_language_venue_description
  FOREIGN KEY (venue_id) REFERENCES project.venue (id) ON DELETE CASCADE;

ALTER TABLE project.review
  ADD CONSTRAINT fk_created_by_review
  FOREIGN KEY (created_by) REFERENCES project.users (id);
ALTER TABLE project.review
  ADD CONSTRAINT fk_edited_by_review
FOREIGN KEY (edited_by) REFERENCES project.users (id);
ALTER TABLE project.review
  ADD CONSTRAINT fk_venue_review
  FOREIGN KEY (venue_id) REFERENCES project.venue (id) ON DELETE CASCADE;

ALTER TABLE project.rating
  ADD CONSTRAINT fk_user_rating
  FOREIGN KEY (created_by) REFERENCES project.users (id);
ALTER TABLE project.rating
  ADD CONSTRAINT fk_venue_rating
  FOREIGN KEY (venue_id) REFERENCES project.venue (id) ON DELETE CASCADE;

ALTER TABLE project.category_venue
  ADD CONSTRAINT fk_category_id
  FOREIGN KEY (category_id) REFERENCES project.category (id);
ALTER TABLE project.category_venue
  ADD CONSTRAINT fk_venue_id
  FOREIGN KEY (venue_id) REFERENCES project.venue (id);

-- Create indexes
CREATE UNIQUE INDEX idx_username_user
  ON project.users (username);
CREATE INDEX idx_created_by_category
  ON project.category (created_by);
CREATE INDEX idx_createdBy_venue
  ON project.venue (created_by);

-- Create procedures
CREATE OR REPLACE FUNCTION project.calculate_rating(ven_id BIGINT)
RETURNS numeric AS $$
DECLARE new_rating NUMERIC;
BEGIN
    new_rating := (SELECT AVG(rating) FROM project.rating WHERE venue_id=ven_id);
    UPDATE project.venue AS venue
        SET rating=(new_rating)
        WHERE id=ven_id;
    RETURN new_rating;
END;
$$ LANGUAGE plpgsql;

-- Insert constants
INSERT INTO project.role (name) VALUES ('ADMIN');
INSERT INTO project.role (name) VALUES ('MODERATOR');
INSERT INTO project.role (name) VALUES ('USER');

INSERT INTO project.language (name) VALUES ('en');
INSERT INTO project.language (name) VALUES ('hr');
INSERT INTO project.language (name) VALUES ('kr');