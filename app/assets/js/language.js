const LANG_CHANGE = '/api/language/change?lang=';

function changeLanguage(lang) {
  $.ajax({
    type: 'GET',
    url: `${LANG_CHANGE + lang}`,
    success: (response) => { location.reload() },
    error: (error) => {
      alert(error.message);
    }
  })
}