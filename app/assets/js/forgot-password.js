$(document).ready((e) =>{
    $("#forgot-form").submit((e) =>{
        e.preventDefault();
        $("#before-reset").attr("hidden", "true");
        $("#after-reset").removeAttr("hidden");

        $.ajax({
            method:"POST",
            url: '/api/reset_password',
            data: {
                email: $("#email").val()
            }
        })
    })
})

