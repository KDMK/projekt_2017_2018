const VENUES_TEMPLATE = '<tr>\n' +
    '<td class="id">{{id}}</td>\n' +
    '<td>{{localizedData.0.name}}</td>\n' +
    '<td>{{address}}</td>\n' +
    '<td>{{email}}</td>\n' +
    '<td>{{phone}}</td>\n' +
    '<td>{{website}}</td>\n' +
    '<td>\n' +
    '    <p data-placement="top" data-toggle="tooltip" title="Edit">\n' +

    '        <button id="update{{id}}" class="btn btn-primary btn-sm" onclick="fillModal({{id}})" data-toggle="modal" data-target="#myUpdateModal" ' +
    '>\n' +
    '            <span class="fa fa-pencil fa-2x"></span>\n' +
    '        </button>\n' +
    '    </p>\n' +
    '</td>\n' +
    '<td>\n' +
    '    <p data-placement="top" data-toggle="tooltip" title="Delete">\n' +
    '        <button class="btn btn-danger btn-sm" onclick="deleteVenueWithId({{id}})"' +
    '>\n' +
    '            <span class="fa fa-trash-o fa-2x"></span>\n' +
    '        </button>\n' +
    '    </p>\n' +
    '</td>\n' +
    '</tr>';

const CATEGORIES_TEMPLATE = '<div class="form-check form-check-inline">\n' +
    '<label class="form-check-label">\n' +
    '<input class="form-check-input" type="checkbox" id="category{{id}}" onclick="updateCheckedCategories(id)">{{localizedData.0.name}}</label>\n' +
    '</div>\n';

const VENUES_CATEGORIES_TEMPLATE = '<div class="form-check form-check-inline">\n' +
    '<label class="form-check-label">\n' +
    '<input class="form-check-input" type="checkbox" id="updateCategory{{id}}" onclick="updateUpdatedCheckedCategories(id)">{{localizedData.0.name}}</label>\n' +
    '</div>\n';

window.onload = () => {
    getVenues();
    getCategories();
    checkSession();
};

let checkedCategories = [];
let updatedCheckedCategoriesOld = [];
let updatedCheckedCategoriesNew = [];

function updateCheckedCategories(_id) {
    _id = _id.substr("category".length);
    const index = checkedCategories.indexOf(_id);
    if (index > -1) {
        checkedCategories.splice(index, 1);
    } else {
        checkedCategories.push(_id);
    }
    console.log(checkedCategories);
}

function updateUpdatedCheckedCategories(_id) {
    _id = _id.substr("updateCategory".length);
    const index = updatedCheckedCategoriesNew.indexOf(_id);
    if (index > -1) {
        updatedCheckedCategoriesNew.splice(index, 1);
    } else {
        updatedCheckedCategoriesNew.push(_id);
    }
    console.log(updatedCheckedCategoriesNew);
}

function fillModal(_id) {
    $.ajax({
        type: 'GET',
        url: '/api/venue/' + _id,
        dataType: 'json',
        success: (resp) => {
            document.getElementById("venue_id").value = resp.data.id;
            document.getElementById("localized_data_id").value = resp.data.localizedData[0].id;
            document.getElementById("name_update").value = resp.data.localizedData[0].name;
            document.getElementById("desc_update").value = resp.data.localizedData[0].description;
            document.getElementById("address_update").value = resp.data.address;
            document.getElementById("website_update").value = resp.data.website;
            document.getElementById("phone_update").value = resp.data.phone;
            document.getElementById("email_update").value = resp.data.email;
        },
        error: (err) => { /* ignore for now */
        }
    });

    $('#update-collapse-categories').empty();
    $.ajax({
        type: 'GET',
        url: '/api/category',
        dataType: 'json',
        success: (resp) => {
            let categories = resp.data;
            categories.sort(comparatorCategoriesById);
            showCategories(categories, VENUES_CATEGORIES_TEMPLATE, '#update-collapse-categories');
        },
        error: (err) => { /* ignore for now */
        }
    }).then(() => {
        $.ajax({
            type: 'GET',
            url: '/api/category-venue/venue/' + _id,
            dataType: 'json',
            success: (resp) => {
                let categoryVenues = resp.data;
                updatedCheckedCategoriesOld = [];
                updatedCheckedCategoriesNew = [];
                for (let categoryVenue of categoryVenues) {
                    updatedCheckedCategoriesOld.push(categoryVenue.category_id);
                    updatedCheckedCategoriesNew.push(categoryVenue.category_id);
                    document.getElementById(`updateCategory${categoryVenue.category_id}`).checked = true;
                }
            },
            error: (err) => { /* ignore for now */
            }
        });
    });
}

function showVenues(_venues) {
    let template = Handlebars.compile(VENUES_TEMPLATE);
    let venueTable = $('#venues-table-body');

    for (let venue of _venues) {
        let venueHtml = template(venue);
        venueTable.append(venueHtml);
    }
}

function showCategories(_categories, TEMPLATE, location) {
    let template = Handlebars.compile(TEMPLATE);
    let venueCategories = $(location);

    for (let category of _categories) {
        let venuesCategoriesHtml = template(category);
        venueCategories.append(venuesCategoriesHtml);
    }
}

function deleteVenueWithId(_id) {
    if (confirm(`Are you sure you want to delete venue (id=${_id})?`)) {
        $.ajax({
            type: 'GET',
            url: `/api/review/venue/${_id}`,
            dataType: 'json',
            error: (err) => { /* ignore for now */
            }
        }).then((resp) => {
            let allVenues = resp.data;
            const exists = allVenues.length > 0;

            $.ajax({
                type: 'DELETE',
                url: `/api/venue/${_id}`,
                dataType: 'json',
                data: {
                    venue_id: _id,
                    venues: exists
                },
                success: (resp) => {
                    toastSuccess(VENUE_DELETED);
                    window.location.reload();
                },
                error: (err) => {
                    toastError(VENUE_NOT_DELETED);
                }
            });
        });
    }
}

function getVenues() {
    $.ajax({
        type: 'GET',
        url: '/api/venue',
        dataType: 'json',
        success: (resp) => {
            let venues = resp.data;
            venues.sort(comparatorVenuesById);
            showVenues(venues);
        },
        error: (err) => { /* ignore for now */
            console.error("Error in venue administration, get venues");
        }
    });
}

function getCategories() {
    $.ajax({
        type: 'GET',
        url: '/api/category',
        dataType: 'json',
        success: (resp) => {
            let categories = resp.data;
            categories.sort(comparatorCategoriesById);
            showCategories(categories, CATEGORIES_TEMPLATE, '#collapse-categories');
        },
        error: (err) => { /* ignore for now */
        }
    });
}

function updateCurrentVenue() {
    const venue_id = $('#venue_id').val();
    let file = $('#file-input-update')[0].files[0];
    let formData = new FormData();

    formData.append("photo", file);

    $.ajax({
        type: 'PUT',
        url: '/api/venue/' + venue_id,
        dataType: 'json',
        data: {
            venue_id: venue_id,
            venue_localized_data_id: document.getElementById("localized_data_id").value,
            venue_name: document.getElementById("name_update").value,
            venue_description: document.getElementById("desc_update").value,
            venue_address: document.getElementById("address_update").value,
            venue_website: document.getElementById("website_update").value,
            venue_phone: document.getElementById("phone_update").value,
            venue_email: document.getElementById("email_update").value,
            venue_categories_old: updatedCheckedCategoriesOld,
            venue_categories_new: updatedCheckedCategoriesNew
        },
        success: (resp) => {
            $.ajax({
                type: 'POST',
                url: '/api/venue/upload/' + venue_id,
                data: formData,
                processData: false,
                contentType: false,
                success: (resp) => {
                    closeModal("#myUpdateModal");
                    toastSuccess(resp.data);
                    window.location.reload();
                },
                error: (err) => {
                    console.log(err);
                }
            });
        },
        error: (err) => { /* ignore for now */
        }
    });

    updatedCheckedCategoriesOld = [];
    updatedCheckedCategoriesNew = [];
}

function createVenue() {
    let croName = $('#name_hr').val();
    let engName = $('#name_en').val();
    let korName = $('#name_kr').val();
    let engDesc = $('#desc_en').val();
    let croDesc = $('#desc_hr').val();
    let korDesc = $('#desc_kr').val();

    let names = [croName, engName, korName];
    let descriptions = [croDesc, engDesc, korDesc];
    let languages = [CROATIAN, ENGLISH, KOREAN];

    let file = $('#file-input')[0].files[0];
    let formData = new FormData();

    formData.append("photo", file);

    $.ajax({
        type: 'POST',
        url: '/api/venue',
        dataType: 'json',
        data: {
            venue_names: names,
            venue_descriptions: descriptions,
            venue_address: document.getElementById("address").value,
            venue_website: document.getElementById("website").value,
            venue_phone: document.getElementById("phone").value,
            venue_email: document.getElementById("email").value,
            venue_languages: languages,
            venue_categories: checkedCategories
        },
        success: (resp) => {
            const venue_id = resp.data.id;
            $.ajax({
                type: 'POST',
                url: '/api/venue/upload/' + venue_id,
                data: formData,
                processData: false,
                contentType: false,
                success: (resp) => {
                    closeModal("#myModal");
                    toastSuccess(resp.data);
                    window.location.reload();
                },
                error: (err) => {
                }
            });
            closeModal("#myModal");
            showVenues([resp.data]);
            toastSuccess(resp.data);
        },
        error: (err) => {
        }
    })

    checkedCategories = [];
}

function emptyModal() {
    $('#name_hr').val("");
    $('#name_en').val("");
    $('#name_kr').val("");
    $('#desc_en').val("");
    $('#desc_hr').val("");
    $('#desc_kr').val("");
    $('#address').val("");
    $('#phone').val("");
    $('#website').val("");
    $('#email').val("");
    $('#collapse-categories').empty();
    getCategories();
}

function comparatorVenuesById(u1, u2) {
    if (Number(u1.id) > Number(u2.id)) return 1;
    if (Number(u1.id) < Number(u2.id)) return -1;
    if (Number(u1.id) === Number(u2.id)) return 0;
}

function comparatorCategoriesById(u1, u2) {
    if (Number(u1.id) > Number(u2.id)) return 1;
    if (Number(u1.id) < Number(u2.id)) return -1;
    if (Number(u1.id) === Number(u2.id)) return 0;
}
