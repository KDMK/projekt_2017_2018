const USER = "user";

const LOGIN_SUCCESS_MESSAGE = `Welcome back!`;
const LOGOUT_SUCCESS_MESSAGE = 'See you next time.';
const REGISTRATION_SUCCESS_MESSAGE = 'Registration complete. Check your mailbox for activation email.';
const PASSWORD_MISMATCH = 'Passwords don\'t match!';
const CATEGORY_ADDED='Category added succesfully.';
const CATEGORY_DELETED='Category deleted succesfully.';
const USER_DELETED='User deleted succesfully.';
const USER_ACTIVATED='User activated succesfully.';
const USER_DEACTIVATED='User deactivated succesfully';
const USER_ROLE_CHANGED='User role succesfully changed.';
const CATEGORY_UPDATED='Category updated succesfully.';
const ROLE_CHANGED = 'Role changed!';