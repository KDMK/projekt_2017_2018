/*
 *  DTO CREATION
 */
function createLoginDTO(_username, _password) {
  let loginDTO = {};
  loginDTO.username = _username;
  loginDTO.password = _password;

  return loginDTO;
}

function createRegistrationDTO(_username, _password, _fullName) {
  let registrationDTO = createLoginDTO(_username, _password);
  registrationDTO.full_name = _fullName;

  return registrationDTO;
}

/*
 *  MODAL CONTROL
 */
function closeModal(name) {
  $(name).modal('toggle');
}

/*
 *  MISC
 */
function createElementFromTemplate(_template, _lang) {
  let elementTemplate = Handlebars.compile(_template);

  let localization;
  try {
    localization = selectLocalization(_lang);
  } catch (ex) {
    // We passed custom localization object(ex. category names or venue names);
    localization = _lang;
  }

  let element = elementTemplate(localization);
  return element;
}