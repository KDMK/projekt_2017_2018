const NAVIGATION = '#navigation';
const NAV_SEPARATOR = '#nav-separator';

const languageArea = '#language-area';
const searchArea = '#search-area';
const logoArea = '#logo-area';

$(window).on("resize", function () {
  if ($(window).width() > 991) {
    $(searchArea).insertBefore($(logoArea));
  } else {
    $(searchArea).insertAfter($(languageArea));
  }
}).resize();

$('#searchInput').keypress((event) => {
  if(event.key === 'Enter') {
    search(event.target.value);
  }
});

$.ajax({
  type: "GET",
  url: "/api/category",
  dataType: 'json',
  success: (response) => {
    let navSeparator = $(NAV_SEPARATOR);

    let categories = transformCategories(response.data);

    for (let key of Object.keys(categories)) {
      if (categories[key].cat.localizedData[0].name.length) {
        let replacement = {
          CATEGORY_ID: categories[key].cat.id,
          CATEGORY_NAME: categories[key].cat.localizedData[0].name
        };

        if (categories[key].children.length !== 0) {
          // create submenu
          navSeparator.before(createElementFromTemplate(DROPDOWN_NAVIGATION_ITEM, replacement));
          let dropdownContent = $(`#cat-${replacement.CATEGORY_ID}`);

          for (let child of categories[key].children) {
            let childRepl = {
              CATEGORY_ID: child.id,
              CATEGORY_NAME: child.localizedData[0].name
            };

            dropdownContent.append(createElementFromTemplate(NAVIGATION_ITEM, childRepl))
          }
        } else {
          navSeparator.before(createElementFromTemplate(NAVIGATION_ITEM, replacement));
        }
      }
    }
  },
  error: (response) => {
    toastError(response.responseJSON);
  },
});

function transformCategories(categories) {
  let transCategories = {};

  for (let category of categories) {
    if (category.parent === null) { // category is root category
      if (typeof transCategories[category.id] === 'undefined') { // check if there is already children; if not create new object
        transCategories[category.id] = {
          cat: category,
          children: []
        };
      } else { // if there already are children update category
        transCategories[category.id].cat = category;
      }
    } else { // category have parent
      if (typeof transCategories[category.parent] === 'undefined') { // parent is not yet created, create only children list
        transCategories[category.parent] = {
          cat: null,
          children: [category]
        }
      } else { // parent exists, append only children
        transCategories[category.parent].children.push(category);
      }
    }
  }

  return transCategories;
}

function search(val) {
  window.location = `/venue/search?term=${val}`;
}

function navbarShow() {
  $('#navigation').addClass('expanded');

  let command = $('#command-area');
  command.addClass('expanded');
  command.removeClass('navbar-nav');
  command.removeClass('navbar-right');
}

function navbarClose() {
  $('#navigation').removeClass('expanded');

  let command = $('#command-area');
  command.removeClass('expanded');
  command.addClass('navbar-nav');
  command.addClass('navbar-right');
}

$(document).ready(function () {

  $('html').click(function () {
    $('#navigation').removeClass('expanded');

    let command = $('#command-area');
    command.removeClass('expanded');
    command.addClass('navbar-nav');
    command.addClass('navbar-right');
  });

  $('.btn-menu').click(function (event) {
    event.stopPropagation();
  });
});