const CATEGORIES_TEMPLATE = '<tr>\n' +
  '<th scope="row">{{id}}</th>\n' +
  '<td>{{localizedData.0.name}}</td>\n' +
  '<td>\n' +
  '    <p data-placement="top" data-toggle="tooltip" title="Edit">\n' +
  '        <button id="update{{id}}" class="btn btn-primary btn-sm" onclick="fillModal({{id}})" data-toggle="modal" data-target="#updateModal" ' +
  '>\n' +
  '            <span class="fa fa-pencil fa-2x"></span>\n' +
  '        </button>\n' +
  '    </p>\n' +
  '</td>\n' +
  '<td>\n' +
  '    <p data-placement="top" data-toggle="tooltip" title="Delete">\n' +
  '        <button class="btn btn-danger btn-sm" onclick="deleteCategory({{id}})"' +
  '>\n' +
  '            <span class="fa fa-trash-o fa-2x"></span>\n' +
  '        </button>\n' +
  '    </p>\n' +
  '</td>\n' +
  '</tr>';


window.onload = () => {
  getCategories();
  checkSession();
};

function fillModal(_id) {
  console.log(_id);
  $.ajax({
    type: 'GET',
    url: '/api/category/' + _id,
    dataType: 'json',
    success: (resp) => {
      console.log(resp.data.localizedData[0].name);
      document.getElementById("categoryName").value = resp.data.localizedData[0].name;
      document.getElementById("categoryId").value = resp.data.localizedData[0].id;
      document.getElementById("id").value = resp.data.id;

    },
    error: (err) => { /* ignore for now */
    }
  });

}

function showCategories(_categories) {
  let template = Handlebars.compile(CATEGORIES_TEMPLATE);
  let categoryTable = $('#category-table-body');

  for (let category of _categories) {
    let categoryHtml = template(category);
    categoryTable.append(categoryHtml);
    $(document).on("click", "#update" + category.id, function () {
    });
  }
}

function deleteCategory(_id) {
  $.ajax({
      type: 'GET',
      url: '/api/category-venue/category/' + _id,
      dataType: 'json',
      error: (err) => { /* ignore for now */
      }
  }).then((resp) => {
    let allVenues = resp.data;
    const exists = allVenues.length > 0;

    $.ajax({
        type: 'DELETE',
        url: '/api/category/' + _id,
        dataType: 'json',
        data: {
            venues: exists
        },
        success: (resp) => {
            window.location.reload();
            toastSuccess(CATEGORY_DELETED);
        },
        error: (err) => { /* ignore for now */
        }
    });
  });
}

function createCategory() {
  let croName = $('#name_hr').val();
  let engName = $('#name_en').val();
  let korName = $('#name_kr').val();

  let names = [croName, engName, korName];
  let languages = [CROATIAN, ENGLISH, KOREAN];

  $.ajax({
    type: 'POST',
    url: '/api/category',
    dataType: 'json',
    data: {
      categoryNames: names,
      categoryLanguages: languages
    },
    success: (resp) => {
      closeModal("#addingModal");
      showCategories([resp.data])
      toastSuccess(CATEGORY_ADDED);
    },
    error: (err) => {
    }
  });
}

function updateCategory() {
  $.ajax({
    type: 'PUT',
    url: '/api/category/' + document.getElementById("id").value,
    dataType: 'json',
    data: {
      category_id: document.getElementById("categoryId").value,
      category_name: document.getElementById("categoryName").value
    },
    success: (resp) => {
      closeModal("#updateModal");
      window.location.reload();
    },
    error: (err) => { /* ignore for now */
    }
  });

}

function comparatorCategoriesById(u1, u2) {
  if (Number(u1.id) > Number(u2.id)) return 1;
  if (Number(u1.id) < Number(u2.id)) return -1;
  if (Number(u1.id) === Number(u2.id)) return 0;
}

function getCategories() {
  $.ajax({
    type: 'GET',
    url: '/api/category',
    dataType: 'json',
    success: (resp) => {
      let categories = resp.data;
      categories.sort(comparatorCategoriesById);
      showCategories(categories);
    },
    error: (err) => { /* ignore for now */
    }
  });
}

function emptyModal() {
    $('#name_hr').val("");
    $('#name_en').val("");
    $('#name_kr').val("");
}