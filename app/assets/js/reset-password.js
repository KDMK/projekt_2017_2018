$(document).ready(() =>{
    $("#reset-form").submit((e1) =>{
        e1. preventDefault();
        $("#newPasswordHelp").attr("hidden", "true");
        $("#confirmPasswordHelp").attr("hidden", "true");
        let password = $("#reset-password").val();
        let confirmPassword = $("#reset-password-confirm").val();
        if (password == '') {
            $("#newPasswordHelp").removeAttr("hidden");
            return;
        }
        if (password != confirmPassword) {
            $("#confirmPasswordHelp").removeAttr("hidden");
            return;
        }

        resetPassword();
    })
})

function getToken() {
    return window.location.href.slice(window.location.href.indexOf('=') + 1);
}

function resetPassword() {
  let _password = $('#reset-password');
  let passwordConfirm = $('#reset-password-confirm');
  let token=getToken();
  
  if (_password.val() != passwordConfirm.val()){
    toastError(PASSWORD_MISMATCH);
    return;
  }

  console.log(_password.val());

  $.ajax({
    type: 'PUT',
    url: '/api/reset_password/'+ token,
    dataType: 'json',
    data: {
        password: _password.val()
    },
    success: (response) => {
      if (response.success) {
        toastSuccess(REGISTRATION_SUCCESS_MESSAGE);
        window.location = "/";
      }
    },
    error: (response) => {
      toastError(response.responseJSON.message);
    }
  });
}