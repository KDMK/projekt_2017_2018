toastSuccess = (message) => {
  $.toast({
    heading: 'Success',
    text: message,
    icon: 'success',
    hideAfter: 3000,
    position: 'top-right'
  });
};

toastError = (message) => {
  $.toast({
    heading: 'Error',
    text: message,
    icon: 'error',
    hideAfter: 3000,
    position: 'top-right'
  });
};

toastWarning = (message) => {
  $.toast({
    heading: 'Warning',
    text: message,
    icon: 'warning',
    hideAfter: 3000,
    position: 'top-right'
  });
};