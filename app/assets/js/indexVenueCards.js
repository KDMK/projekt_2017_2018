const VENUE_CARDS_TEMPLATE =
    '<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-xs-12">' +
    '       <div class="card" >' +
    '           <div class="card-custom-header" ><a class="nav-link" href="/venue/{{id}}">{{localizedData.0.name}}</a></div >' +
    '           <div class="row" >' +
    '                <div class="col-md-6" >' +
    '                   <img id="venue-image-{{id}}" src="/images/no_image.png" />' +
    '                    <div class="number-rating" >{{rating}}/5 </div >' +
    '                </div >' +
    '              <div class="col-md-6 venue-info" >' +
    '                    <div class="description-venue" >{{localizedData.0.description}}</div >' +

    '                   <div class="rating" id="rate-{{id}}">' +
    '<span onclick="rate({{id}}, 5)">☆</span>' +
    '<span onclick="rate({{id}}, 4)">☆</span>' +
    '<span onclick="rate({{id}}, 3)">☆</span>' +
    '<span onclick="rate({{id}}, 2)">☆</span>' +
    '<span onclick="rate({{id}}, 1)">☆</span>' +
    '                    </div >' +
    '                </div >' +
    '            </div >' +
    '       </div >' +
    '   </div >';

window.onload = () => {
    if(typeof query_term == 'undefined') {
        getVenues();
    } else {
        searchVenues(query_term);
    }
    checkSession();
};

function showVenues(_venues) {
    let template = Handlebars.compile(VENUE_CARDS_TEMPLATE);
    let venueTable = $('#index-venue-cards');

    for (let venue of _venues) {
        let venueHtml = template(venue);
        venueTable.append(venueHtml);

        if(venue.image != null) {
            $(`#venue-image-${venue.id}`).attr("src",`/res/venue/image/${venue.image}`);
        }

        if (localStorage.getItem(USER) === null) {
            $(`#rate-${venue.id}`).toggle();
        }
    }
}

function comparatorVenuesById(u1, u2) {
    if (u1.id > u2.id) return 1;
    if (u1.id < u2.id) return -1;
    if (u1.id === u2.id) return 0;
}

function getVenues() {
    $.ajax({
        type: 'GET',
        url: '/api/venue',
        dataType: 'json',
        success: (resp) => {
            let venues = resp.data;
            venues.sort(comparatorVenuesById);
            showVenues(venues);
        },
        error: (err) => { /* ignore for now */
            console.error("Error in venue administration, get venues");
        }
    });
}

function searchVenues(query) {
    $.ajax({
        type: 'GET',
        url: '/api/venue/search?term=' + query,
        dataType: 'json',
        success: (resp) => {
            let venues = resp.data;
            venues.sort(comparatorVenuesById);
            showVenues(venues);
        },
        error: (err) => { /* ignore for now */
            console.error("Error in venue administration, get venues");
        }
    });
}

function rate(venue_id, rating) {
    $.ajax({
        type: 'POST',
        url: `/api/venue/rate`,
        dataType: 'json',
        data: {venue_id:venue_id, rating:rating},
        success: (response) => {
            toastSuccess("Vote registered.")
            window.location.reload();
        },
        error: (response) => {
            toastError("You are not authorized to vote.");
        },
    });
}
