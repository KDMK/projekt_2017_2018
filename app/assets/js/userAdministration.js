const USER_TEMPLATE = '<tr>\n' +
    '<th scope="row">{{id}}</th>\n' +
    '<td>{{full_name}}</td>\n' +
    '<td>{{username}}</td>\n' +
    '<td>\n' +
    '    <div class="form-group">\n' +
    '        <select onchange="changeRole({{id}})" name="role{{id}}" id="role{{id}}" value="{{role.name}}" class="form-control">\n' +
    '            <option value="ADMIN">Administrator</option>\n' +
    '            <option value="MODERATOR">Moderator</option>\n' +
    '            <option value="USER">User</option>\n' +
    '        </select>\n' +
    '    </div>\n' +
    '</td>\n' +
    '<td>\n' +
    '    <p data-placement="top" data-toggle="tooltip" title="Activate" >\n' +
    '      <button class="btn btn-outline-success btn-sm" id="button-activate-{{id}}" onclick="activate({{id}})">\n' +
    '          <span class="fa fa-check-circle-o fa-2x"></span>\n' +
    '      </button>\n' +
    '    </p>\n' +
    '</td>\n' +
    '<td>\n' +
    '    <p data-placement="top" data-toggle="tooltip" title="Deactivate">\n' +
    '        <button class="btn btn-outline-danger btn-sm" id="button-deactivate-{{id}}" onclick="deactivate({{id}})"' +
    '>\n' +
    '            <span class="fa fa-ban fa-2x"></span>\n' +
    '        </button>\n' +
    '    </p>\n' +
    '</td>\n' +
    '<td>\n' +
    '    <p data-placement="top" data-toggle="tooltip" title="Delete">\n' +
    '        <button class="btn btn-danger btn-sm" onclick="deleteUser({{id}})"' +
    '>\n' +
    '            <span class="fa fa-trash-o fa-2x"></span>\n' +
    '        </button>\n' +
    '    </p>\n' +
    '</td>\n' +
    '</tr>';

function activate(_id) {
    $.ajax({
        type: "PUT",
        url: '/api/user/' + _id,
        data: {
            change: {
                "active": true
            }
        },
        success: (response) => {
            if (response.success) {
                toastSuccess(USER_ACTIVATED);
                toggleActivate(_id);
                toggleDeactivate(_id);
            }
        },
        error: (response) => {
        }
    });

}

function deactivate(_id) {
    $.ajax({
        type: "PUT",
        url: '/api/user/' + _id,
        data: {
            change: {
                "active": false
            }
        },
        success: (response) => {
            if (response.success) {
                toastSuccess(USER_DEACTIVATED);
                toggleActivate(_id);
                toggleDeactivate(_id);
            }
        },
        error: (response) => {
            console.log(response);
        }
    });
}

function deleteUser(_id) {
    $.ajax({
        type: "DELETE",
        url: '/api/user/' + _id,
        success: (response) => {
            toastSuccess(USER_DELETED);
            window.location.reload();
        },
        error: (response) => {
            console.log(response);
        }
    });
}

window.onload = () => {
    getUsers();
    checkSession();
};

function showUsers(_users) {
    let template = Handlebars.compile(USER_TEMPLATE);
    let userTable = $('#user-table-body');

    for (let user of _users) {
        let userHtml = template(user);
        userTable.append(userHtml);
        user.active ? toggleActivate(user.id) : toggleDeactivate(user.id);
        $('#role' + user.id + ' option[value=' + user.role.name + ']').prop('selected', 'selected');
    }
}

function toggleActivate(_id) {
    $('#button-activate-' + _id).toggle();
}

function toggleDeactivate(_id) {
    $('#button-deactivate-' + _id).toggle();
}

function comparatorUsersById(u1, u2) {
    if (Number(u1.id) > Number(u2.id)) return 1;
    if (Number(u1.id) < Number(u2.id)) return -1;
    if (Number(u1.id) === Number(u2.id)) return 0;
}

function changeRole(_id) {
    let role = $('#role' + _id).val();
    let role_id;
    if (role == 'ADMIN') role_id = 1;
    else if (role == 'MODERATOR') role_id = 2;
    else if (role == 'USER') role_id = 3;
    console.log(role_id);
    $.ajax({
        type: "PUT",
        url: '/api/user/' + _id,
        data: {
            change: {
                "role_id": role_id,
            }
        },
        success: (response) => {
            toastSuccess(USER_ROLE_CHANGED);
        },
        error: (response) => {
        }
    });

}

function getUsers() {
    $.ajax({
        type: 'GET',
        url: '/api/user',
        dataType: 'json',
        success: (resp) => {
            let users = resp.data;
            users.sort(comparatorUsersById);
            showUsers(users);
        },
        error: (err) => { /* ignore for now */
        }
    });
}