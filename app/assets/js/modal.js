var registeredModals = [];

function registerModal(name, btnTrigger, btnClose) {
  var modal = document.getElementById(name);
  var trigger = document.getElementById(btnTrigger);
  var close = document.getElementById(btnClose);

  var newModal = {
    modal: modal,
    trigger: trigger,
    close: close
  };

  trigger.onclick = function () {
    modal.style.display = "block";
  }

  close.onclick = function () {
    modal.style.display = "none";
  };

  window.onclick = function (event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }

  registeredModals.push(newModal);
}
