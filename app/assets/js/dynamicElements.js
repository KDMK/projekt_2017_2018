const ABOUT_BUTTON = '<li class="nav-item"><a class="nav-link" href="/about">{{ABOUT}}</a></li>';
const SIGN_IN_BUTTON = '<li class="nav-item"><a href="#" class="nav-link" data-toggle="modal" data-target="#auth-modal">{{LOGIN}}</a></li>';
const SIGN_OUT_BUTTON = '<li class="nav-item"><a href="#" class="nav-link" data-toggle="modal" onclick="logout()">{{LOGOUT}}</a></li>';

const NAVIGATION_ITEM = '<li class="nav-item"><a class="nav-link" href="/venue/get?category_id={{CATEGORY_ID}}">{{CATEGORY_NAME}}</a></li>';

const ADMIN_BUTTON = '<button type="button" class="btn btn-link">\n    <a href="/admin/user"><img class="language-flag" src="/images/admin.png"/></a>\n</button>';

const DROPDOWN_NAVIGATION_ITEM = '<li class="nav-item dropdown">\n' +
  '    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{CATEGORY_NAME}}\n        <span class="caret"></span></a>\n' +
  '    <ul class="dropdown-menu" id="cat-{{CATEGORY_ID}}">\n    ' +
  '</ul>\n' +
  '</li>';