const VENUE_TEMPLATE = `<div class="row justify-content-center venue-title">{{localizedData.0.name}}</div>
<div class="row image-map-wrapper">
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
        <div class="row justify-content-center image-map">
            <div class="row">
                <div class="col-sm-12">
                    <img src="http://www.zagreb.hr/UserDocsImages//dogadanja/1200-4.jpg"
                         class="rounded  mx-auto d-block"
                         id="image"/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="rating-number">{{RATING}}:&nbsp;<span id="rating-num">{{rating}}</span>&nbsp;<span style="color: gold;">★</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="rating" id="rate">
                        <span onclick="rate({{id}}, 5)">☆</span>
                        <span onclick="rate({{id}}, 4)">☆</span>
                        <span onclick="rate({{id}}, 3)">☆</span>
                        <span onclick="rate({{id}}, 2)">☆</span>
                        <span onclick="rate({{id}}, 1)">☆</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
        <div id="map"></div>
        <div class="venue-address">{{address}}</div>
    </div>
</div>
<div class="row description-info-wrapper">
    <div class="col-lg-6">
        <p class="venue-description">
            {{localizedData.0.description}}
        </p>
    </div>
    <div class="col-lg-6 info-wrapper">
        <hr>
        <div class="row justify-content-center venue-info">{{website}}</div>
        <div class="row justify-content-center venue-info">{{phone}}</div>
        <div class="row justify-content-center venue-info">{{email}}</div>
        <hr>
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="col-lg-12">
            <div class="comments" id="all-reviews">
                <!--<hr/>-->
                <span id="commie" style="display:none"></span>
            </div>
            <div class="add-comment" id="add-comment">
                <textarea class="textarea-input ti-comment" id="review_content"
                          placeholder="{{COMMENT_AREA}}"></textarea>
                <input type="button" class="btn btn-green float-right" onclick="createReview({{id}})"
                       value="{{COMMENT_BUTTON}}">
            </div>
        </div>
    </div>
</div>`;


const REVIEWS_TEMPLATE = `<div id="review-{{id}}">
<div class="comment" >
    <div class="comment-user-info">
        <div class="profile-pic-wrap ppw-small">
            <img src="data:image/png;base64,{{avatar}}">
        </div>
        <a class="comment-author"><i class="fa fa-user fa-fw"
                                                              aria-hidden="true"></i>
            {{user}}
        </a>
        <button class="btn btn-sm btn-danger fa fa-trash" id="delete-review" onclick="deleteReview({{id}})" hidden ></button>   
    </div>
    <div class="comment-content-wrap">
        <p class="comment-content">{{content}}</p>
        <span class="comment-date">{{datetime}}</span>
    </div>
</div>
<hr> 
</div>`;

window.onload = () => {
  getVenue(venueId);
  checkSession();
  
  
};

function showVenue(_venue) {
  let template = Handlebars.compile(VENUE_TEMPLATE);
  let venueTable = $('#venue-page');

  let venueHtml = template(Object.assign(_venue, selectLocalization(document.documentElement.lang)));
  venueTable.append(venueHtml);
  initMap(_venue.address);

  if (_venue.image != null) {
    $('#image').attr("src", `/res/venue/image/${_venue.image}`);
  }

  if (localStorage.getItem(USER) === null) {
    $('#rate').toggle();
    $('#add-comment').toggle();
  }

  getReviews(_venue.id);
  
}

function getSession(){
  $.ajax({
    type:'GET',
    url:'/api/session',
    dataType:'json',
    success: (resp) => {
      if (!resp.passport && localStorage.getItem(USER)) {
        localStorage.removeItem(USER);
      }
      showButtons(JSON.parse(localStorage.getItem(USER)));
    },
    error: (response) => {
      toastError(response.responseJSON.message);
    },
  });
}

function showButtons(user){
  console.log(user);
  let userRole;
  if (user != null) {
    userRole = user.role.id;
  }
  if(userRole=='1' || userRole=='2'){
    $('*[id*=delete-review]').each(function () {
      $(this).removeAttr("hidden");
    });
  }
}

function getVenue(_venue_id) {
  $.ajax({
    type: 'GET',
    url: `/api/venue/${_venue_id}`,
    dataType: 'json',
    success: (response) => {
      let venue = response.data;
      showVenue(venue);
    },
    error: (response) => {
      toastError(response.responseJSON.message);
    },
  });
}

function initMap(address) {
  $.ajax({
    type: 'GET',
    url: `https://maps.googleapis.com/maps/api/geocode/json?address=${address ? address.replace(new RegExp(' ', 'g'), '%20') : "Zagreb"}&key=AIzaSyDipRXIkoaYCLn_Jixh6q615E8I3BPLZs0`,
    success: (response) => {

      let lat = response.results[0].geometry.location.lat;
      let lng = response.results[0].geometry.location.lng;

      let options = {
        zoom: 17,
        center: {lat: lat, lng: lng}
      };
      let map = new google.maps.Map(document.getElementById('map'), options);
      let marker = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        map: map
      });
    },
    error: (response) => {
      toastError(response.responseJSON.message);
    },
  });
}

function getReviews(_venueId) {
  $.ajax({
    type: 'GET',
    url: `/api/review/venue/${_venueId}`,
    dataTaype: 'json',
    success: (response) => {
      let reviews = response.data;
      showReviews(reviews);
    }
  })
}

function showReviews(_reviews) {
  let template = Handlebars.compile(REVIEWS_TEMPLATE);

  for (let review of _reviews) {
    const hash = md5(review.user);

    review.datetime = new Date(review.timestamp).toLocaleString();
    review.avatar = new Identicon(hash).toString();
    console.log(review.id);

    let reviewHtml = template(review);

    let rev = $(reviewHtml);

    rev.insertBefore('#commie');
    
  }
  getSession();
}

function createReview(_venueId) {
  let reviewContent = $('#review_content');

  $.ajax({
    type: 'POST',
    url: '/api/review',
    dataType: 'json',
    data: {
      venue_id: _venueId,
      content: reviewContent.val()
    },
    success: (resp) => {
      reviewContent.val("");
      showReviews([resp.data]);
    },
    error: (err) => {
      toastError(err.status == 401 ? 'Unauthorized' : err.message);
    }
  });
}

function deleteReview(_reviewId){
  
  console.log(_reviewId);
  $.ajax({
    
    type: 'DELETE',
    url:'/api/review/'+_reviewId,
    dataType:'json',
    data:{
      reviewId:_reviewId
    },
    success: (resp) => {

      toastSuccess("Review deleted.");
      //getReviews(location.href.split("venue/")[1];
      $('#review-'+_reviewId).remove();
    },
    error: (resp) => {
      console.log(resp);
      
    }
  });
}

function rate(venue_id, rating) {
  $.ajax({
      type: 'GET',
      url: `/api/venue/rate/user/${venue_id}`,
      dataType: 'json',
      success: (response) => {
          if (response.data === null) {
              $.ajax({
                  type: 'POST',
                  url: `/api/venue/rate`,
                  dataType: 'json',
                  data: {venue_id:venue_id, rating:rating},
                  success: (response) => {
                	  $("#rating-num").html(new Number(response.data.rating).toFixed(2));
                      toastSuccess("Vote registered.");
                  },
                  error: (response) => {
                      toastError("You are not authorized to vote.");
                  },
              });
          } else {
              toastError("You already voted for this venue.")
          }
      },
      error: (response) => {
          console.log(response);
          toastError("You are not authorized to vote.");
      },
  });
}
