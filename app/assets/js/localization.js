const KOREAN = 'kr';
const CROATIAN = 'hr';
const ENGLISH = 'en';

const HR_LOCALIZATION = {
  ABOUT: 'Opis',
  LOGIN: 'Prijavi se',
  LOGOUT: 'Odjavi se',
  RATING: 'Ocjena',
  COMMENT_BUTTON: 'Predaj',
  COMMENT_AREA: "Upišite komentar"
};

const EN_LOCALIZATION = {
  ABOUT: 'About',
  LOGIN: 'Sign in',
  LOGOUT: 'Sign out',
  RATING: 'Rating',
  COMMENT_BUTTON: 'Submit',
  COMMENT_AREA: "Type comment"
};

const KR_LOCALIZATION = {
  ABOUT: '기술',
  LOGIN: '로그인',
  LOGOUT: '로그 아웃',
  RATING: '평가',
  COMMENT_BUTTON: '제출하다',
  COMMENT_AREA: "댓글 입력"
};

/*"COMMENT_BUTTON": "Post",
"COMMENT_FIELD": "Comment...",
"COMMENT_TITLE": "Reviews"
*/

function selectLocalization(lang) {
  switch (lang) {
    case KOREAN:
      return KR_LOCALIZATION;
    case CROATIAN:
      return HR_LOCALIZATION;
    case ENGLISH:
      return EN_LOCALIZATION;
    default:
      throw new Error('No such localization');
  }
}