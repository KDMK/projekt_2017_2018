const MODAL_REF = '#auth-modal';
const COMMAND_AREA = '#command-area';
const LANGUAGE_AREA = '#language-area';
const ADMIN_AREA = '#nav-list-admin';
const BACKGROUND_IMAGES = ['background2.jpg', 'background.jpg'];
let backgroundIndex = 0;

$(document).ready(() => {
  setInterval(() => {
    if (backgroundIndex >= BACKGROUND_IMAGES.length) backgroundIndex = 0;
    $('#background').css('background-image', `url(/images/${BACKGROUND_IMAGES[backgroundIndex++]})`);
  }, 7000);
});

window.onload = () => {
  checkSession();
};

function renderButtons(user) {
  let userRole;
  if (user != null) {
    userRole = user.role.name;
  }

  let lang = document.documentElement.lang;
  let commandArea = $(COMMAND_AREA);
  let languageArea = $(LANGUAGE_AREA);
  commandArea.html('');


  if (userRole) {
    if (userRole !== 'USER') {
      languageArea.append(createElementFromTemplate(ADMIN_BUTTON, lang));
    }
    commandArea.append(createElementFromTemplate(SIGN_OUT_BUTTON, lang));
  } else {
    commandArea.append(createElementFromTemplate(ABOUT_BUTTON, lang));
    commandArea.append(createElementFromTemplate(SIGN_IN_BUTTON, lang));
  }
}

function checkSession() {
  $.ajax({
    type: 'GET',
    url: '/api/session',
    dataType: 'json',
    success: (resp) => {
      // Session expired -> clear user from local cahce now
      if (!resp.passport && localStorage.getItem(USER)) {
        localStorage.removeItem(USER);
      }
      renderButtons(JSON.parse(localStorage.getItem(USER)));
    },
    error: (err) => { /* ignore for now */
    }
  });
}

function login() {
  let username = document.getElementById('login-email');
  let password = document.getElementById('login-password');

  let loginDTO = createLoginDTO(username.value, password.value);

  $.ajax({
    type: 'POST',
    url: '/api/auth/login',
    dataType: 'json',
    data: loginDTO,
    success: (response) => {
      if (response.success) {
        toastSuccess(LOGIN_SUCCESS_MESSAGE);
        localStorage.setItem(USER, JSON.stringify(response.data));
        console.log(response);
        renderButtons(response.data);
        resetInputs(username, password);
        closeModal(MODAL_REF);
        location.reload();
      }
    },
    error: (response) => {
      toastError(response.responseJSON.message);
      resetInputs(username, password);
    }
  });
}

function registration() {
  let fullName = document.getElementById('register-full-name');
  let username = document.getElementById('register-email');
  let password = document.getElementById('register-password');
  let passwordConfirm = document.getElementById('register-password-confirm');

  if (password.value !== passwordConfirm.value) {
    toastError(PASSWORD_MISMATCH);
    return;
  }
  $.ajax({
    type: 'POST',
    url: '/api/auth/register',
    dataType: 'json',
    data: createRegistrationDTO(username.value, passwordConfirm.value, fullName.value),
    success: (response) => {
      if (response.success) {
        toastSuccess(REGISTRATION_SUCCESS_MESSAGE);
        resetInputs(fullName, username, password, passwordConfirm);
        closeModal(MODAL_REF);
      }
    },
    error: (response) => {
      toastError(response.responseJSON.message);
      resetInputs(username, password);
    }
  });
}

function resetInputs() {
  for (let input of arguments) {
    input.value = null;
  }
}

function logout() {
  $.ajax({
    type: 'GET',
    url: '/api/auth/logout',
    dataType: 'json',
    success: (response) => {
      if (response.success) {
        localStorage.removeItem(USER);
        toastSuccess(LOGOUT_SUCCESS_MESSAGE);
        renderButtons();
        window.location.replace('/');
      }
    },
    error: (response) => {
      toastError(response.responseJSON.message);
    }
  });
}