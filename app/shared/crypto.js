module.exports = () => {
  const bcrypt = require('bcrypt');

  function _comparePasswords(userPassword, databasePassword) {
    return bcrypt.compareSync(userPassword, databasePassword);
  }

  function _hashPassword(password) {
    const salt = bcrypt.genSaltSync();
    const hash = bcrypt.hashSync(password, salt);

    return hash;
  }

  this.comparePassword = _comparePasswords;
  this.hashPassword = _hashPassword;

  return this;
};