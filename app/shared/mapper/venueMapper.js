const mapUser = require('./userMapper').mapUser;

function _venueCategoryMapper(venueCategory) {
  if (!venueCategory) return null;

  let retVal = {};

  retVal = _venueMapper(venueCategory.venue);

  return retVal;
}

function _venueMapper(venue) {
  if (!venue) return null;

  let retVal = {};

  retVal.id = venue.id;
  retVal.website = venue.website;
  retVal.address = venue.address;
  retVal.phone = venue.phone;
  retVal.email = venue.email;
  retVal.image = venue.image;
  retVal.rating = new Number(venue.rating).toFixed(2);
  retVal.creator = mapUser(venue.creator);
  retVal.editor = mapUser(venue.editor);
  retVal.localizedData = venue.venue_localized_data.map(_venueLocalizedDataMapper);

  return retVal;
}

function _venueLocalizedDataMapper(localizedData) {
  if (!localizedData) return null;

  let venueLocalizedData = {};
  venueLocalizedData.id = localizedData.id;
  venueLocalizedData.name = localizedData.name;
  venueLocalizedData.description = localizedData.description;

  let language = {};
  language.id = localizedData.language.id;
  language.name = localizedData.language.name;

  venueLocalizedData.language = language;

  return venueLocalizedData;
}

function _mapReview(_review) {
  if(!_review) return null;

  let review = {};
  review.user = _review.creator.username;
  review.content = _review.content;
  review.timestamp = Date.parse(_review.created_at);
  review.id=_review.id;

  return review;
}

module.exports = {
  mapVenue: _venueMapper,
  mapVenueCategory: _venueCategoryMapper,
  mapReview: _mapReview
};