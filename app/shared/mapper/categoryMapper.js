const userMapper = require('./userMapper');

function _mapLocalizedData(elem) {
  if(!elem) return null;

  let categoryLocalizedData = {};
  categoryLocalizedData.id = elem.id;
  categoryLocalizedData.name = elem.name;

  let language = {};
  language.id = elem.language.id;
  language.name = elem.language.name;

  categoryLocalizedData.language = language;

  return categoryLocalizedData;
}

function _categoryMapper(elem) {
  if(!elem) return null;

  let category = elem.toJSON();
  let mappedCategory = {};

  mappedCategory.id = category.id;
  mappedCategory.parent = category.parent;
  mappedCategory.created_by = userMapper.mapUser(category.creator);
  mappedCategory.edited_by = userMapper.mapUser(category.editor);

  mappedCategory.localizedData = category.category_localized_data.map(_mapLocalizedData);

  return mappedCategory;
}

module.exports = {
  mapCategory: _categoryMapper
};