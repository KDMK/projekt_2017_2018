const mapUser = require('./userMapper').mapUser;

function _reviewMapper(review) {
  if (!review) return null;

  let retVal = {};

  retVal.id = review.id;
  retVal.venue_id = review.venue_id;
  retVal.title = review.title;
  retVal.content = review.content;
  retVal.creator= mapUser(review.created_by);
  retVal.editor = mapUser(review.edited_by);

  return retVal;
}



module.exports = {
  mapReview: _reviewMapper,
};