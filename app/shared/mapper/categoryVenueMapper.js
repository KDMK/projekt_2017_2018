function _categoryVenueMapper(categoryVenue) {
    if (!categoryVenue) return null;

    let retVal = {};

    retVal.id = categoryVenue.id;
    retVal.category_id = categoryVenue.category_id;
    retVal.venue_id = categoryVenue.venue_id;
    return retVal;
}

module.exports = {
    mapCategoryVenue: _categoryVenueMapper
};
