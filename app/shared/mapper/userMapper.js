function _userMapper(user) {
  if (!user) return null;

  let retVal = {};
  let role = {};

  retVal.id = user.id;
  retVal.username = user.username;
  retVal.full_name = user.full_name;
  retVal.role = role;
  retVal.active = user.active;

  role.id = user.role.id;
  role.name = user.role.name;

  return retVal;
}

module.exports = {
  mapUser: _userMapper
};