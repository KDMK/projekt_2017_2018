const handleResponse = require('../domain/local/responseData').handleResponse;
const mapUser = require('../shared/mapper/userMapper').mapUser;

module.exports = (passport, dao) => {
  function _login(req, res, next) {
    return passport.authenticate('local', (err, user, info) => {
      if (err) {
        handleResponse(res, 500, err, null);
        return;
      }
      if (!user) {
        handleResponse(res, 404, 'User not found', null);
        return;
      }
      if (user) {
        req.logIn(user, function (err) {
          if (err) {
            handleResponse(res, 500, 'Server error', err);
            return;
          }
          handleResponse(res, 200, 'Succesfully logged in.', user);
        });
      }
    })(req, res, next);
  }

  function _register(req, res, next) {
    return passport.createUser(req)
      .then((response) => {
        passport.authenticate('local', (err, user, info) => {
          if (user) {
            handleResponse(res, 200, 'Succesfully registered.', user);
          }
        })(req, res, next);
      })
      .catch((err) => {
        handleResponse(res, 400, err.message, err);
      });
  }

  function _logout(req, res, next) {
    req.logout();
    handleResponse(res, 200, 'Succesfully logged out.');
  }

  function _getUsers(req, res, next) {
    dao.user.getUsers()
      .then((users) => {
        handleResponse(res, 200, "", users.map(mapUser));
      })
      .catch((err) => {
        handleResponse(res, 500, `Error getting users.`, err.message);
      });
  }

  function _deleteUser(req, res) {
    dao.user.deleteUser(req.params.user_id)
      .then((users) => {
        handleResponse(res, 200, "", null);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error getting users.`, err.message);
      });
  }

  function _updateUser(req, res) {
    let change = req.body.change;
    let userId = req.params.user_id;
    dao.user.updateUser(userId, change)
      .then((user) => {
        handleResponse(res,200, `Succesfully updated user with id ${userId}.`, null);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error updating user with id ${userId}`, err);
      });
  }

  module.login = _login;
  module.register = _register;
  module.logout = _logout;
  module.getUsers = _getUsers;
  module.deleteUser = _deleteUser;
  module.updateUser = _updateUser;

  return module;
}