const handleResponse =  require('../domain/local/responseData').handleResponse;
const mapReview = require('../shared/mapper/venueMapper').mapReview;

module.exports = (dao) => {
	
	this.getReviews = (req, res) => {
		dao.review.getAllReviews(req.session.lang)
		  .then((reviews) => {
		    handleResponse(res, 200, "", reviews.map(mapReview));
		  })
		  .catch((err) => {
		    handleResponse(res, 500, `Error getting reviews.`, err);
		  });
	};

	this.deleteReview= (req, res) => {
		console.log("ISPIS JE!!!!!!!!!!!!!!!!!!!!!!!!!!"+req.body.reviewId);
		dao.review.deleteReview(req.body.reviewId)
			.then((reviews) => {
				handleResponse(res, 200, "", null);
			})
			.catch((err) => {
				handleResponse(res, 500, `Error deleting review.`, err.message);
			})
	};

	this.getReviewsByVenueId = (req, res) => {
		dao.review.getReviewsByVenueId(req.params.venue_id)
			.then((reviews) => {
				handleResponse(res, 200, "", reviews.map(mapReview));
			})
			.catch((err) => {
				handleResponse(res, 500, `Error getting reviews for venue_id ${req.params.venue_id}.`, err.message);
			})
	};

	this.createReview = (req, res) => {
		dao.review.createReview(req.user.id, req.body.venue_id, req.body.content)
      .then((review) => {
        handleResponse(res, 200, "", mapReview(review));
      })
      .catch((err) => {
        handleResponse(res, 500, `Error creating review for venue_id ${req.body.venue_id}.`, err);
      })
	};
	
	return this;
};