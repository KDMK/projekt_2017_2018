const handleResponse = require('../domain/local/responseData').handleResponse;
const mapCategoryVenue = require('../shared/mapper/categoryVenueMapper').mapCategoryVenue;

module.exports = (dao) => {

    this.getCategoriesVenues = (req, res) => {
        dao.category_venue.getAllCategoriesVenues()
            .then((categoryVenue) => {
                handleResponse(res, 200, "", categoryVenue.map(mapCategoryVenue));
            })
            .catch((err) => {
                handleResponse(res, 500, `Error getting categories_venues.`, err.message);
            });
    };

    this.getVenuesByCategoryId = (req, res) => {
        dao.category_venue.getAllVenuesByCategoryId(req.params.category_id)
            .then((venues) => {
                handleResponse(res, 200, "", venues.map(mapCategoryVenue));
            })
            .catch((err) => {
                handleResponse(res, 500, `Error getting venues.`, err.message);
            });
    };

    this.getCategoriesByVenueId = (req, res) => {
        console.log(req.params.venue_id)
        dao.category_venue.getAllCategoriesByVenueId(req.params.venue_id)
            .then((categories) => {
                handleResponse(res, 200, "", categories.map(mapCategoryVenue));
            })
            .catch((err) => {
                handleResponse(res, 500, `Error getting categories.`, err.message);
            });
    };

    return this;
};
