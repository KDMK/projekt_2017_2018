const HR_LOCALIZATION = require('../localization/hr.json');
const KR_LOCALIZATION = require('../localization/kr.json');
const EN_LOCALIZATION = require('../localization/en.json');

const KOREAN = 'kr';
const CROATIAN = 'hr';
const ENGLISH = 'en';

const ASSETS = 'assets';
const NOT_FOUND = '/404.html';
const HOME = 'index.handlebars';
const ABOUT_PAGE = 'about.handlebars';
const USER_ADMINISTRATION = 'user-administration.handlebars';
const CATEGORY_ADMINISTRATION = 'categories-administration.handlebars';
const VENUE_ADMINISTRATION = 'venue-administration.handlebars';
const VENUE = 'venue.handlebars';
const SEARCH_VENUE = 'search-result.handlebars';
const VENUE_PAGE = 'venue-page.handlebars';
const FORGOT_PASSWORD='forgot-password.handlebars';
const RESET_PASSWORD='reset-password.handlebars';

const Handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

function _selectLocalization(lang) {
    switch (lang) {
        case KOREAN:
            return KR_LOCALIZATION;
        case CROATIAN:
            return HR_LOCALIZATION;
        case ENGLISH:
            return EN_LOCALIZATION;
        default:
            throw new Error('No such localization');
    }
}

module.exports = () => {
    this.serveStatic = (req, res) => {
        res.sendFile(ASSETS + req.originalUrl, {'root': './app'});
    };

    this.serveMainPage = (req, res) => {
        let filePath = path.join(__dirname, '..', ASSETS, HOME);
        fs.readFile(filePath, 'utf8', (error, content) => {
            const template = Handlebars.compile(content);

            res.status(200).send(template(_selectLocalization(req.session.lang)));
        });
    };

    this.serveAboutPage = (req, res) => {
        let filePath = path.join(__dirname, '..', ASSETS, ABOUT_PAGE);
        fs.readFile(filePath, 'utf8', (error, content) => {
            const template = Handlebars.compile(content);

            res.status(200).send(template(_selectLocalization(req.session.lang)));
        });
    };

    this.serveForgotPassword = (req, res) => {
        let filePath = path.join(__dirname, '..', ASSETS, FORGOT_PASSWORD);
        fs.readFile(filePath, 'utf8', (error, content) => {
            const template = Handlebars.compile(content);

            res.status(200).send(template(_selectLocalization(req.session.lang)));
        });
    };

    this.serveResetPassword = (req, res) => {
        let filePath = path.join(__dirname, '..', ASSETS, RESET_PASSWORD);
        fs.readFile(filePath, 'utf8', (error, content) => {
            const template = Handlebars.compile(content);

            res.status(200).send(template(_selectLocalization(req.session.lang)));
        });
    };

    this.serveUserAdministration = (req, res) => {
        let filePath = path.join(__dirname, '..', ASSETS, USER_ADMINISTRATION);
        fs.readFile(filePath, 'utf8', (error, content) => {
            const template = Handlebars.compile(content);

            res.status(200).send(template(_selectLocalization(req.session.lang)));
        });
    };

    this.serve404 = (req, res) => {
        res.sendFile(ASSETS + NOT_FOUND, {'root': './app'});
    };

    this.serveCategoryAdministration = (req, res) => {
        let filePath = path.join(__dirname, '..', ASSETS, CATEGORY_ADMINISTRATION);
        fs.readFile(filePath, 'utf8', (error, content) => {
            const template = Handlebars.compile(content);

            res.status(200).send(template(_selectLocalization(req.session.lang)));
        });
    };

    this.serveVenues = (req, res) => {
        let filePath = path.join(__dirname, '..', ASSETS, VENUE);
        fs.readFile(filePath, 'utf8', (error, content) => {
            const template = Handlebars.compile(content);

            let data = _selectLocalization(req.session.lang);
            data.category_id = req.query.category_id;

            res.status(200).send(template(data));
        });
    };

    this.serveVenue = (req, res) => {
        let filePath = path.join(__dirname, '..', ASSETS, VENUE_PAGE);
        fs.readFile(filePath, 'utf8', (error, content) => {
            const template = Handlebars.compile(content);

            let data = _selectLocalization(req.session.lang);
            data.map_lang = req.session.lang === 'kr' ? 'ko' : req.session.lang;
            data.venue_id = req.params.venue_id;

            res.status(200).send(template(data));
        });
    };

  this.serveSearchVenue = (req, res) => {
    let filePath = path.join(__dirname, '..', ASSETS, SEARCH_VENUE);
    fs.readFile(filePath, 'utf8', (error, content) => {
      const template = Handlebars.compile(content);

      let data = _selectLocalization(req.session.lang);
      data.query = req.query.term;

      res.status(200).send(template(data));
    });
  };

    this.serveVenueAdministration = (req, res) => {
        let filePath = path.join(__dirname, '..', ASSETS, VENUE_ADMINISTRATION);
        fs.readFile(filePath, 'utf8', (error, content) => {
            const template = Handlebars.compile(content);

            res.status(200).send(template(_selectLocalization(req.session.lang)));
        });
    };

    return this;
};