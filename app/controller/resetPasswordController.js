const handleResponse = require('../domain/local/responseData').handleResponse;
const mapUser = require('../shared/mapper/userMapper').mapUser;
const nodemailer = require('nodemailer');
const crypto=require('crypto');

module.exports = (dao) => {
    function _sendResetMail(req, res) {
        email = req.body.email;
        console.log('Email adresa:'+email);

        //TODO provjeriti postoji li email u bazi, ako postoji dohvati korisnika kojem pripada
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            auth:{
                user: process.env.MAILER_USERNAME,
                pass: process.env.MAILER_PASSWORD
            }
        });

        let token = crypto.randomBytes(20).toString('hex');

        dao.user_password.addToken(token, email);

        const resetUrl = process.env.RESET_URL;
        // setup email data with unicode symbols
        let mailOptions = {
            from: 'no-reply@hedonistic-zagreb.com', // sender address
            to: email, // list of receivers
            subject: 'Password Reset', // Subject line
            text: 'Click on this link to reset your password.', // plain text body
            html: `<a href="http://${resetUrl}/reset_password?token=${token}">Click on this link to reset your password.</a>` // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
        });

    }

    function _resetPassword(req, res){
        let token=req.params.token;
        let password=req.body.password;

        dao.user_password.changePassword(token, password)
        .then((user) => {
            handleResponse(res, 200, "", user);
          })
          .catch((err) => {
            handleResponse(res, 500, `Error changing password.`, err.message);
          });
    }

    module.resetPassword=_resetPassword;
    module.sendResetMail= _sendResetMail;
    return module;
};