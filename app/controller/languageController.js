const handleResponse = require('../domain/local/responseData').handleResponse;

module.exports = (dao) => {
  this.getLanguages = (req, res) => {
    dao.language.getLanguages()
      .then((languages) => {
        handleResponse(res, 200, "", languages);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error getting languages.`, err.message);
      });
  };

  this.getLanguage = (req, res) => {
    dao.language.getLanguage(req.params.language_id)
      .then((language) => {
        handleResponse(res, 200, "", language);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error fetching language with id ${req.params.language_id}.`,
          err.message);
      });
  };

  this.createLanguage = (req, res) => {
    let name = req.param('languageName');

    if (!name) {
      handleResponse(res, 400, 'Name is mandatory to create new language.', null);
      return;
    }

    dao.language.createLanguage(name)
      .then((language) => {
        handleResponse(res, 200, "", language);
      })
      .catch((err) => {
        handleResponse(res, 500, 'Error creating language.', err.message);
      });
  };

  this.updateLanguage = (req, res) => {
    dao.language.updateLanguage(req.params.language_id, req.body.language_name)
      .then((changedRows) => {
        handleResponse(res, 200, `Successfully changed ${changedRows} rows.`, null);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error editing language with id ${req.params.language_id}.`,
          err.message);
      });
  };

  this.deleteLanguage = (req, res) => {
    dao.language.deleteLanguage(req.params.language_id)
      .then((deletedRows) => {
        handleResponse(res, 200, `Successfully deleted ${deletedRows} rows.`, null);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error deleting language with id ${req.params.language_id}.`,
          err.message);
      });
  };

  this.changeLanguage = (req, res) => {
    let selectedLanguage = req.query.lang;

    dao.language.getLanguageByName(selectedLanguage)
      .then((lang) => {
        if (lang) {
          req.session.lang = lang.name;
          handleResponse(res, 200, `Succesfully switched language to ${selectedLanguage}.`, null);
        } else {
          handleResponse(res, 400, `There is no such language: ${selectedLanguage}`, null);
        }
      })
      .catch((err) => {
        handleResponse(res, 500, err.message, err);
      })
  };

  return this;
};