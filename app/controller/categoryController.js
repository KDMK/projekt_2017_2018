const handleResponse = require('../domain/local/responseData').handleResponse;
const categoryMapper = require('../shared/mapper/categoryMapper');

module.exports = (dao) => {
  this.getCategories = (req, res) => {
    dao.category.getAllCategories(req.session.lang)
      .then((categories) => {
        handleResponse(res, 200, "", categories.map(categoryMapper.mapCategory));
      })
      .catch((err) => {
        handleResponse(res, 500, `Error getting categories.`, err.message);
      });
  };

  this.getCategory = (req, res) => {
    dao.category.getCategoryById(req.params.category_id, req.session.lang)
      .then((category) => {
        handleResponse(res, 200, "", categoryMapper.mapCategory(category))
      })
      .catch((err) => {
        handleResponse(res, 500, `Error fetching category with id ${req.params.category_id}.`,
          err.message);
      });
  };

  this.getCategoryByName = (req, res) => {
    dao.category.getCategoryByName(req.query.name, req.session.lang)
      .then((category) => {
        handleResponse(res, 200, "", categoryMapper.mapCategory(category));
      })
      .catch((err) => {
        handleResponse(res, 500, `Error fetching category with name ${req.query.name}.`,
          err.message);
      });
  };

  this.createCategory = (req, res) => {
    let name = req.param('categoryNames');
    let language = req.param('categoryLanguages');

    if (!name || !language) {
      handleResponse(res, 400, 'Name and language are mandatory to create new category.', null);
      return;
    }

    dao.category.createCategory(req.user.id, name, language, req.session.lang)
      .then((category) => {
        handleResponse(res, 200, "", categoryMapper.mapCategory(category));
      })
      .catch((err) => {
        handleResponse(res, 500, 'Error creating category.', err.message);
      });
  };

  this.updateCategory = (req, res) => {
    dao.category.updateCategory(req.body.category_id, req.body.category_name)
      .then((changedRows) => {
        handleResponse(res, 200, `Successfully changed ${changedRows} rows.`, null);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error editing category with id ${req.body.category_id}.`,
          err.message);
      });
  };

  this.deleteCategory = (req, res) => {
      dao.category.deleteCategory(req.params.category_id, req.body.venues)
      .then((deletedRows) => {
        handleResponse(res, 200, `Successfully deleted ${deletedRows} rows.`, null);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error deleting category with id ${req.params.category_id}.`,
          err.message);
      });
  };

  return this;
};