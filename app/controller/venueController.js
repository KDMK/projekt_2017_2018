const STORAGE_ROOT = process.env.HOME + '/projectUploads/venue';
const handleResponse = require('../domain/local/responseData').handleResponse;
const mapVenue = require('../shared/mapper/venueMapper').mapVenue;
const fs = require('fs');
const path = require('path');
const mapVenueCategory = require('../shared/mapper/venueMapper').mapVenueCategory;

module.exports = (dao) => {
  this.getVenues = (req, res) => {
    dao.venue.getAllVenues(req.session.lang)
      .then((venues) => {
        handleResponse(res, 200, "", venues.map(mapVenue));
      })
      .catch((err) => {
        handleResponse(res, 500, `Error getting venues.`, err);
      });
  };

  this.getVenueById = (req, res) => {
    dao.venue.getVenueById(req.params.venue_id, req.session.lang)
      .then((venue) => {
        handleResponse(res, 200, "", mapVenue(venue));
      })
      .catch((err) => {
        console.log(err);
        handleResponse(res, 500, `Error getting venue with id.`, err);
      });
  };

  this.getVenuesByCategory = (req, res) => {
    // TODO: dohvatiti venue po kategoriji
    dao.venue.getVenuesByCategoryId(req.query.category_id, req.session.lang)
      .then((venueCategory) => {
        handleResponse(res, 200, "", venueCategory.map(mapVenueCategory));
      })
      .catch((err) => {
        console.log(err);
        handleResponse(res, 500, `Error getting venues.`, err);
      });
  };

  this.createVenue = (req, res) => {
    let venueData = {};
    venueData.address = req.body.venue_address;
    venueData.phone = req.body.venue_phone;
    venueData.website = req.body.venue_website;
    venueData.email = req.body.venue_email;
    venueData.categories = req.body.venue_categories;

    let name = req.body.venue_names;
    let description = req.body.venue_descriptions;
    let language = req.body.venue_languages;

    dao.venue.createVenue(req.user.id, language, name, description, req.session.lang, venueData)
      .then((venue) => {
        handleResponse(res, 200, "", mapVenue(venue));
      })
      .catch((err) => {
        handleResponse(res, 500, 'Error creating venue.', err);
      });
  };

  this.uploadFile = (req, res) => {
    let venueData = {};
    let fileName = req.file.originalname.split('.');
    venueData.fileName = `${fileName[0]}${Date.now()}.${fileName[1]}`; // adding timestamp prevents duplicate names(so that we don't have to check that also) :)

    if (!fs.existsSync(STORAGE_ROOT)) {
      createRootDir();
    }

    fs.writeFile(`${STORAGE_ROOT}/${venueData.fileName}`.replace(new RegExp(" ", "g"), ""), req.file.buffer, (err) => {
      if (err) {
        // TODO: Sto napraviti u slucaju neuspjesnog uploada venue avatara? Treba li i to biti obuhvaceno 'transakcijom'?
        handleResponse(res, 500, 'Error creating venue.', err);
      }
      dao.venue.updateVenueImage(req.user.id, req.params.venue_id, venueData.fileName)
        .then((changedRows) => {
          handleResponse(res, 200, `Successfully changed ${changedRows} rows.`, null);
        })
        .catch((err) => {
          handleResponse(res, 500, `Error editing venue with id ${req.params.venue_id}.`, err.message);
        });
    });
  };

  this.deleteVenue = (req, res) => {
    dao.venue.deleteVenue(req.params.venue_id, req.body.venues)
      .then((deletedRows) => {
        handleResponse(res, 200, `Successfully deleted ${deletedRows} rows.`, null);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error deleting category with id ${req.params.category_id}.`,
          err.message);
      });
  };

  this.updateVenue = (req, res) => {
    let venueData = {};
    venueData.name = req.body.venue_name;
    venueData.description = req.body.venue_description;
    venueData.address = req.body.venue_address;
    venueData.phone = req.body.venue_phone;
    venueData.website = req.body.venue_website;
    venueData.email = req.body.venue_email;
    venueData.categories_old = req.body.venue_categories_old;
    venueData.categories_new = req.body.venue_categories_new;

    dao.venue.updateVenue(req.user.id, req.params.venue_id, req.body.venue_localized_data_id, venueData)
      .then((changedRows) => {
        handleResponse(res, 200, `Successfully changed ${changedRows} rows.`, null);
      })
      .catch((err) => {
        handleResponse(res, 500, `Error editing venue with id ${req.params.venue_id}.`,
          err.message);
      });
  };

  this.searchVenue = (req, res) => {
    dao.venue.searchVenue(req.query.term, req.session.lang)
      .then((resultSet) => {
        handleResponse(res, 200, "", resultSet.map(mapVenue));
      })
      .catch((err) => {
        handleResponse(res, 500, `Error occured while searching for venues`, err.message);
      })
  };

  this.rateVenue = (req, res) => {
    dao.rating.saveRating(req.body.venue_id, req.body.rating, req.user.id)
        .then((rating) => {
          dao.triggerUpdateRating(rating.venue_id)
              .then((result) => {
                  handleResponse(res, 200, "", rating);
              })
              .catch((error) => {
                handleResponse(res, 500, "", error.message);
              });
        })
        .catch((err) => {
            handleResponse(res, 500, `Error occured while rating venue`, err.message);
        })
  };

  this.getRateForUser = (req, res) => {
    dao.rating.getRateForUser(req.params.venue_id, req.user.id)
        .then((rating) => {
            if (rating === null) {
              handleResponse(res, 200, "", null);
            } else {
              handleResponse(res, 200, "", rating);
            }
        })
        .catch((err) => {
            handleResponse(res, 500, `Error occured while getting user's rate for venue`, err.message);
        })
  };

  function createRootDir() {
    STORAGE_ROOT.split('/').reduce((parentDir, childDir) => {
      const curDir = path.resolve(parentDir, childDir);

      try {
        fs.mkdirSync(curDir);
      } catch(err) {
        if (err.code !== 'EEXIST') {
          throw err;
        }
      }

      return curDir;
    }, '/');
  }

  return this;
};