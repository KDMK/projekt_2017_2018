module.exports = (venue, venueLocalizedData, rating, review, users, role, category, language, categoryLocalizedData, venueCategory) => {
  const VENUE_LOCALIZED_DATA = {
    model: venueLocalizedData,
    include: [
      {
        model: language,
        where: {name: ''}
      }
    ]
  };

  const CREATOR = {
    as: 'creator',
    model: users,
    include: [role]
  };

  const EDITOR = {
    as: 'editor',
    model: users,
    include: [role]
  };

  const VENUE = {
    as: 'venue',
    model: venue,
    include: [VENUE_LOCALIZED_DATA, CREATOR, EDITOR]
  };

  function setLanguage(_lang) {
    // TODO: Find nicer way to do this
    VENUE_LOCALIZED_DATA.include[0].where.name = _lang;
  }

  function getLanguageId(_lang) {
    return language.findOne({where: {name: _lang}});
  }

  /**
   * Gets all categories from database. Referenced values are mapped to appropriate objects.
   *
   * @returns {Promise<TInstance[]> | Promise.<Array.<Model>>}
   * @private
   */
  this.getAllVenues = (_lang) => {
    setLanguage(_lang);
    return venue.findAll({include: [VENUE_LOCALIZED_DATA, CREATOR, EDITOR]});
  };

  this.getVenueById = (_id, _lang) => {
    setLanguage(_lang);
    return venue.findById(_id, {include: [VENUE_LOCALIZED_DATA, CREATOR, EDITOR]});
  };

  this.getVenuesByCategoryId = (_categoryId, _lang) => {
    setLanguage(_lang);
    return venueCategory.findAll({where: {category_id: _categoryId}, include: [VENUE]});
  };

  this.deleteVenue = (_id, _venues) => {
    if (_venues === "true") {
        return Promise.reject("This venue contains reviews and cannot be deleted.");
    }

    return venueCategory.destroy({where: {venue_id: _id}}).then(() => {
      return venueLocalizedData.destroy({where: {venue_id: _id}}).then(() => {
        return venue.destroy({where: {id: _id}});
      });
    });
  };

  this.updateVenue = (userId, venueId, localizedDataId, data) => {
    return venue
      .update({
        address: data.address,
        email: data.email,
        website: data.website,
        phone: data.phone,
        edited_by: userId
      }, {where: {id: venueId}})
      .then(() => {
        return venueLocalizedData.update(
          {name: data.name, description: data.description}, {where: {id: localizedDataId}});
      })
      .then((resp) => {
        let categoriesOld = data.categories_old;
        let categoriesNew = data.categories_new;

        categoriesNew.filter(category => !categoriesOld.includes(category))
          .forEach(category => {
            return venueCategory.create({venue_id: venueId, category_id: category});
          });
        categoriesOld.filter(category => !categoriesNew.includes(category))
          .forEach(category => {
            return venueCategory.destroy({where: {venue_id: venueId, category_id: category}});
          });

        return resp;
      });
  };

  this.updateVenueImage = (userId, venueId, filePath) => {
    return venue.update({image: filePath, edited_by: userId}, {where: {id: venueId}});
  };

  this.createVenue = (userId, _languages, _names, _descriptions, _lang, data) => {
    if (!Array.isArray(_names)) {
      return Promise.reject('_names expected Array but got ' + typeof _names);
    }

    if (!Array.isArray(_descriptions)) {
      return Promise.reject('_descriptions expected Array but got ' + typeof _descriptions);
    }

    if (!Array.isArray(_languages)) {
      return Promise.reject('_languages expected Array but got ' + typeof _languages);
    }

    return language.findAll({where: {name: _languages}}).then((languages) => {

      if (_names.length !== languages.length
        || _descriptions.length !== languages.length
        || _names.indexOf("") > -1
        || _descriptions.indexOf("") > -1) {
        return Promise.reject('All names and descriptions must be specified.');
      }

      return venue.create({created_by: userId, edited_by: userId}).then((newVenue) => {
        let promises = [];

        for (let i = 0; i < languages.length; i++) {
          let index = _languages.indexOf(languages[i].name);
          promises.push(venueLocalizedData.create({
            venue_id: newVenue.id,
            name: _names[index],
            description: _descriptions[index],
            language_id: languages[i].id
          }));
        }

        promises.push(venue.update({
          address: data.address,
          website: data.website,
          phone: data.phone,
          email: data.email
        }, {where: {id: newVenue.id}}));

        let categories = data.categories;
        for (let i = 0; i < data.categories.length; i++) {
          promises.push(venueCategory.create({venue_id: newVenue.id, category_id: categories[i]}));
        }

        return Promise.all(promises)
          .then(() => {
            return this.getVenueById(newVenue.id, _lang);
          })
          .catch((err) => {
            console.log(err);
            this.deleteVenue(newVenue.id);
            return Promise.reject(err);
          });
      })
    }).catch(err => {
      console.log(err);
    });
  };

  this.searchVenue = (_query, _lang) => {
    setLanguage(_lang);
    return getLanguageId(_lang)
      .then((lang) => {
        console.log(lang.id);
        return venueLocalizedData.findAll({
          where: {
            $or: [{
              name: {ilike: `%${_query}%`},
              language_id: lang.id
            }, {description: {ilike: `%${_query}%`}, language_id: lang.id}]
          }
        })
          .then((results) => {
            let queries = results.map((res) => {
              return {"id": res.venue_id}
            });
            console.log(queries);
            return venue.findAll({where: {$or: queries}, include: [VENUE_LOCALIZED_DATA, CREATOR, EDITOR]});
          })
          .catch(err => {
            console.log(err);
          })
      });
  };

  return this;
};