module.exports = (language) => {
  /**
   * Persists language in database.
   *
   * @param _name language title
   * @returns Promise<Language> created language instance
   */
  this.createLanguage = (_name) => {
    return language.create({name: _name});
  };

  /**
   * Returns language with given id.
   *
   * @param _id of an language
   * @returns {Promise.<TResult>}
   */
  this.getLanguage = (_id) => {
    return language.findById(_id);
  };

  /**
   * Returns language with given name from database.
   *
   * @param _name
   */
  this.getLanguageByName = (_name) => {
    return language.findOne({where: {name: _name}});
  };

  /**
   * Returns all languages from database.
   *
   * @returns {Promise.<TResult>} List of all languages
   */
  this.getLanguages = () => {
    return language.findAll();
  };

  /**
   * Updates an language with given id.
   *
   * @param _id of an language
   * @param _name new name
   * @returns {Promise.<TResult>}
   */
  this.updateLanguage = (_id, _name) => {
    return language.update({name: _name}, {where: {id: _id}});
  };

  /**
   * Deletes language with given id.
   *
   * @param _id of an language
   * @returns {Promise.<undefined>}
   */
  this.deleteLanguage = (_id) => {
    return language.destroy({where: {id: _id}});
  };

  return this;
};