module.exports = (users, role) => {
  this.getUser = (_id) => {
    return users.findById(_id, {include: [role]});
  };

  this.getUsers = () => {
    return users.findAll({include: [role]});
  };

  this.getByUsername = (_username) => {
    return users.findOne({where: {username: _username}, include: [role]});
  };

  this.insertUser = (_username, _password, _fullName, _role) => {
    return role.findOne({where: {name: _role}})
      .then((role) => {
        if (role) {
          return users.create({
            username: _username,
            password: _password,
            full_name: _fullName,
            role_id: role.id
          });
        } else {
          return Promise.reject({message:`No such role ${_role}`, result: role});
        }
      });
  };

  this.deleteUser = (_userId) => {
    return users.destroy({where: {id: _userId}});
  };

  this.updateUser = (_userId, _change) => {
    return users.update(_change, {where: {id: _userId}})
  }

  return this;
};