const crypto = require('../shared/crypto')();

module.exports = (users) => {

  this.addToken = (_token, _email) => {
    return users.update({token: _token}, {where: {username: _email}})
  };

  this.changePassword = (_token, _password) => {
    return users.update({password: crypto.hashPassword(_password), token: null}, {where: {token: _token}})
  };

  return this;
};
