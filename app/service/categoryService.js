module.exports = (category, categoryLocalizedData, language, users, role) => {
  const CATEGORY_LOCALIZED_DATA = (_lang) => {
    this.model = categoryLocalizedData;
    this.include = [{
      model: language,
      where: {name: _lang},
    }];

    return this;
  };

  const CREATOR = {
    as: 'creator',
    model: users,
    include: [role]
  };

  const EDITOR = {
    as: 'editor',
    model: users,
    include: [role]
  };

  /**
   * Gets category with given id.
   *
   * @param _id
   * @returns {Promise.<Model>|Promise<any | TInstance>}
   * @private
   */
  function _getCategoryById(_id, _lang) {
    return category.findById(_id, {include: [CATEGORY_LOCALIZED_DATA(_lang), CREATOR, EDITOR]});
  }

  /**
   * Gets category with given name.
   *
   * @param _name category name
   * @returns {Promise.<TResult>}
   * @private
   */
  function _getCategoryByName(_name, _lang) {
    return categoryLocalizedData.findOne({where: {name: _name}}).then((localized_name) => {
      if (localized_name) {
        return _getCategoryById(localized_name.category_id);
      } else {
        return null;
      }
    });
  }

  /**
   * Gets all categories from database. Referenced values are mapped to appropriate objects.
   *
   * @returns {Promise<TInstance[]> | Promise.<Array.<Model>>}
   * @private
   */
  function _getAllCategories(_lang) {
    return category.findAll({include: [CATEGORY_LOCALIZED_DATA(_lang), CREATOR, EDITOR]});
  }

  /**
   * Creates new article with given localized names. User is expected to provide names and languages
   * as arrays. It's expected that names and languages are ordered in arrays respectively
   * (ex. names = ['Night life', 'Nocni zivot'], languages = ['en', 'hr']
   *
   * @param _userId creator id
   * @param _names list of localized names
   * @param _languages list of languages for which to create names for
   * @returns {Promise.<TResult>}
   */

// TODO: Find better way to define names and languages with objects so that user can pass arguments
// in any order

  function _createCategory(_userId, _names, _languages, _lang) {
    if (!Array.isArray(_names)) {
      return Promise.reject('_names expected Array but got ' + typeof _names);
    }

    if (!Array.isArray(_languages)) {
      return Promise.reject('_languages expected Array but got ' + typeof _languages);
    }

    return language.findAll({where: {name: _languages}}).then((languages) => {
      return category.create({created_by: _userId, edited_by: _userId}).then((newCategory) => {
        let promises = [];

        for (let i = 0; i < languages.length; i++) {
          let index = _languages.indexOf(languages[i].name);
          promises.push(categoryLocalizedData.create({
            category_id: newCategory.id,
            name: _names[index],
            language_id: languages[i].id
          }));
        }

        return Promise.all(promises)
          .then(() => {
            return _getCategoryById(newCategory.id, _lang);
          })
          .catch((err) => {
            _deleteCategory(newCategory.id);
            return Promise.reject(err);
          });
      })
    });
  }

  /**
   * Deletes category with given id.
   *
   * @param _id
   * @private
   */
  function _deleteCategory(_id, _venues) {
    if (_venues === "true") {
        return Promise.reject("This category contains venues and cannot be deleted.");
    }

    return categoryLocalizedData.destroy({where: {category_id: _id}}).then(() => {
      return category.destroy({where: {id: _id}});
    });
  }

  function _updateCategoryName(_localizedId, _newName) {
    return categoryLocalizedData.update({name: _newName}, {where: {id: _localizedId}});
  }

  module.getCategoryById = _getCategoryById;
  module.getCategoryByName = _getCategoryByName;
  module.getAllCategories = _getAllCategories;
  module.createCategory = _createCategory;
  module.deleteCategory = _deleteCategory;
  module.updateCategory = _updateCategoryName;

  return module;
}