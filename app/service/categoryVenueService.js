module.exports = (categoryVenue) => {

    this.getAllCategoriesVenues = () => {
        return categoryVenue.findAll();
    };

    this.getAllVenuesByCategoryId = (_id) => {
        return categoryVenue.findAll({where: {category_id: _id}});
    };

    this.getAllCategoriesByVenueId = (_id) => {
        return categoryVenue.findAll({where: {venue_id: _id}});
    };

    return this;
}
