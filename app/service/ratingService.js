module.exports = (rating) => {
    this.saveRating = (venue_id, rate, user_id) => {
        return rating.create({venue_id: venue_id, rating: rate, created_by:user_id, edited_by: user_id});
    };

    this.getRateForUser = (venue_id, user_id) => {
      return rating.findOne({where: {venue_id: venue_id, created_by: user_id}});
    };

    return this;
};