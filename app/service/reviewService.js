module.exports = (review, users, venue, venueLocalizedData, language, role) => {

  const CREATOR = {
    as: 'creator',
    model: users,
    include: [role]
  };

  this.getReview = (_id) => {
    return review.findById(_id, {include: [CREATOR]});
  };

  this.getAllReviews = () => {
    return review.findAll({include: [CREATOR]});
  };

  this.getReviewsByVenueId = (_venueId) => {
    return review.findAll({where: {venue_id: _venueId}, include: [CREATOR]});
  };

  this.createReview = (_user_id, _venue_id, _content) => {
    if (!_user_id) {
      return Promise.reject('_user_id is mandatory and requires type of number, but got' + typeof _user_id);
    }

    if (!_venue_id) {
      return Promise.reject('_venue_id is mandatory and requires type of number, but got' + typeof _venue_id);
    }

    if (!_content) {
      return Promise.reject('_content is mandatory and requires type of string, but got' + typeof _content);
    }

    return review.create({created_by: _user_id, edited_by: _user_id, venue_id: _venue_id, content: _content}).then((res) => this.getReview(res.id));
  };

  this.deleteReview=(_reviewId)=>{
    return review.destroy({where: {id: _reviewId}});
  }

  return this;
};