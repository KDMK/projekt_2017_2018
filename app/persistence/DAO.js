// This application uses Sequelizejs ORM framework for easier data persistance. If you want to learn
// more about it go to http://docs.sequelizejs.com
//
// For a quick reference please visit http://docs.sequelizejs.com/identifiers.html
const ERROR_CONNECTING_TO_DB = -1;
const databaseConfig = {
  host: process.env.DATABASE_HOST,
  dialect: process.env.DATABASE_DIALECT,
  dialectOptions: {
    ssl: process.env.DATABASE_SSL === 'true',
  },
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  schema: process.env.DATABASE_SCHEMA,
  logging: process.env.DATABASE_LOGGING === "console.log" ? console.log : false
};

function _triggerRating(venue_id) {
    return database.query(`SELECT project.calculate_rating(${venue_id});`);
}

/**
 * Module models database access object and provides all methods for creating and manipulating
 * data. Each table is exported as member with their names respectively. Each table object contains
 * methods for manipulating data along with table definition.
 *
 */
module.exports = () => {
  const Sequelize = require('sequelize');
  database = new Sequelize(process.env.DATABASE_NAME, process.env.DATABASE_USER,
    process.env.DATABASE_PASSWORD, databaseConfig);

  // Import models
  const category = require('../domain/category')(database, Sequelize.DataTypes);
  const categoryLocalizedData = require('../domain/category_localized_data')(database, Sequelize.DataTypes);
  const language = require('../domain/language')(database, Sequelize.DataTypes);
  const rating = require('../domain/rating')(database, Sequelize.DataTypes);
  const review = require('../domain/review')(database, Sequelize.DataTypes);
  const role = require('../domain/role')(database, Sequelize.DataTypes);
  const users = require('../domain/users')(database, Sequelize.DataTypes);
  const venue = require('../domain/venue')(database, Sequelize.DataTypes);
  const venueLocalizedData = require('../domain/venue_localized_data')(database, Sequelize.DataTypes);
  const categoryVenue = require('../domain/category_venue')(database, Sequelize.DataTypes);

  // Create associations
  category.belongsTo(users, {as: 'creator', foreignKey: 'created_by'});
  category.belongsTo(users, {as: 'editor', foreignKey: 'edited_by'});
  category.hasMany(categoryLocalizedData, {foreignKey: 'category_id', onDelete: 'cascade'});

  categoryLocalizedData.belongsTo(language, {foreignKey: 'language_id'});
  venueLocalizedData.belongsTo(language, {foreignKey: 'language_id'});

  language.hasMany(categoryLocalizedData, {foreignKey: 'language_id'});
  language.hasOne(venueLocalizedData, {foreignKey: 'language_id'});

  users.belongsTo(role);

  categoryVenue.belongsTo(category, {foreignKey: 'category_id'});
  categoryVenue.belongsTo(venue, {foreignKey: 'venue_id'});
  category.hasMany(categoryVenue, {foreignKey: 'category_id', onDelete: 'cascade'});
  venue.hasMany(categoryVenue, {foreignKey: 'venue_id', onDelete: 'cascade'});

  venue.hasMany(venueLocalizedData, {foreignKey: 'venue_id', odDelete: 'cascade'});
  venue.belongsTo(users, {as: 'creator', foreignKey: 'created_by'});
  venue.belongsTo(users, {as: 'editor', foreignKey: 'edited_by'});

  rating.belongsTo(venue, {foreignKey: 'venue_id'});
  rating.belongsTo(users, {as: 'creator', foreignKey: 'created_by'});
  rating.belongsTo(users, {as: 'editor', foreignKey: 'edited_by'});

  review.belongsTo(venue, {foreignKey: 'venue_id'});
  review.belongsTo(users, {as: 'creator', foreignKey: 'created_by'});
  review.belongsTo(users, {as: 'editor', foreignKey: 'edited_by'});

  const userService = require('../service/userService')(users, role);
  const userPasswordService = require('../service/passwordService')(users);
  const categoryService = require('../service/categoryService')(category, categoryLocalizedData, language, users, role);
  const categoryVenueService = require('../service/categoryVenueService')(categoryVenue);
  const languageService = require('../service/languageService')(language);
  const venueService = require('../service/venueService')(venue, venueLocalizedData, rating, review, users, role, category, language, categoryLocalizedData, categoryVenue);
  const ratingService = require('../service/ratingService')(rating);
  const reviewService = require('../service/reviewService')(review, users, venue, venueLocalizedData, language, role);

  this.user = userService;
  this.user_password = userPasswordService;
  this.category = categoryService;
  this.category_venue = categoryVenueService;
  this.language = languageService;
  this.venue = venueService;
  this.review = reviewService;
  this.rating = ratingService;
  this.category_venue = categoryVenueService;
  this.testConnection = testConnection.bind(database);
  this.dropDatabase = dropDatabase.bind(database);
  this.triggerUpdateRating = _triggerRating;

  return this;
};

/**
 * Method for testing connection to database. In case of unsuccessful authentication application
 * will terminate execution.
 *
 */
function testConnection(req, res) {
  this.authenticate()
    .then(() => {
      return res.status(200).json({
        success: true,
        message: 'Connection to database has been established successfully.',
        data: null
      });
    })
    .catch(err => {
      return res.status(500).json({
        success: false,
        message: 'Cannot connect to database!',
        data: err
      });
    });
}

/**
 *  Method used for dropping entire database schema.
 *
 *  WARNING: Do not use this is you don't know what are  you doing and you are sure you
 *  want to do it! This method is intended for using in testing environment.
 */
function dropDatabase() {
  this.database.sync({force: true});
}