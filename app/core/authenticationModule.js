const NOT_ACTIVE = 'You are not active. Please contact administrator for further questions';

const LocalStrategy = require('passport-local').Strategy;
const mapUser = require('../shared/mapper/userMapper').mapUser;
const handleResponse = require('../domain/local/responseData').handleResponse;
const crypto = require('../shared/crypto')();

const options = {};

module.exports = (passport, dao) => {
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    dao.user.getUser(id)
      .then((user) => {
        done(null, mapUser(user.toJSON()));
      })
      .catch((err) => {
        done(err, null);
      });
  });

  passport.use('local', new LocalStrategy(options, (username, password, done) => {
    dao.user.getByUsername(username)
      .then((user) => {
        if (!user) return done(null, false);
        if (!user.active) return done(NOT_ACTIVE, false);
        if (!crypto.comparePassword(password, user.password)) {
          return done(null, false);
        } else {
          return done(null, mapUser(user));
        }
      })
      .catch((err) => {
        return done(err);
      });
  }));

  passport.createUser = (req, res) => {
    // TODO: User is created with user role and can be later changed to ADMIN/MODERATOR ??
    return dao.user.insertUser(req.body.username, crypto.hashPassword(req.body.password),
      req.body.full_name, "USER");
  };

  passport.loginRequired = (req, res, next) => {
    if (!req.user) return res.status(401).sendFile('assets/401.html', {'root': './app'});
    return next();
  };

  passport.logoutFirst = (req, res, next) => {
    if (req.user) return handleResponse(res, 405, 'Please log out first.', null);
    return next();
  };
};

