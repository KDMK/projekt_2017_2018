const ROLES = {
  MODERATOR: 'MODERATOR',
  ADMINISTRATOR: 'ADMIN'
};

module.exports = () => {
  this.authorizeAdmin = (req, res, next) => {
    let userRole = req.user.role.name;

    if(userRole !== ROLES.ADMINISTRATOR) {
      return res.status(403).sendFile('assets/403.html', {'root': './app'});
    }

    return next();
  };

  this.authorizeAny = (req, res, next) => {
    let userRole = req.user.role.name;

    for (let key of Object.keys(ROLES)) {
      if (userRole === ROLES[key]) {
        return next();
      }
    }

    return res.status(403).sendFile('assets/403.html', {'root': './app'});
  };

  return this;
};