const express = require('express');
const router = express.Router();
const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

function _defaultLanguage(req, resp, next) {
  if (!req.session.lang) {
    req.session.lang = 'en';
  }
  next();
}

module.exports = (dao, passport) => {
  const authorizationModule = require('./authorizationModule')();
  const venueController = require('../controller/venueController')(dao);
  const categoryController = require('../controller/categoryController')(dao);
  const categoryVenueController = require('../controller/categoryVenueController')(dao);
  const languageController = require('../controller/languageController')(dao);
  const userController = require('../controller/userController')(passport, dao);
  const staticController = require('../controller/staticController')();
  const resetPasswordController=require('../controller/resetPasswordController')(dao);
  const reviewController = require('../controller/reviewController')(dao);

  router.use(_defaultLanguage);

  // Static page routes
  // TODO: Put html in 'protected' folder and serve it this way. Make js, css, and all other assets public(or even consider how to serve it without directly exposing it to client)
  router.get('/', staticController.serveMainPage);
  router.get('/about', staticController.serveAboutPage);
  router.get('/admin/user', staticController.serveUserAdministration);
  router.get('/admin/venue', staticController.serveVenueAdministration);
  router.get('/admin/category', staticController.serveCategoryAdministration);
  router.get('/venue/get', staticController.serveVenues);
  router.get('/venue/search', staticController.serveSearchVenue);
  router.get('/venue/:venue_id', staticController.serveVenue);
  router.get('/forgot_password', staticController.serveForgotPassword);
  router.get('/reset_password', staticController.serveResetPassword);

  // Test DB connection
  router.get('/api/database', dao.testConnection);

  // Venue routes
  router.get('/api/venue', venueController.getVenues);
  router.get('/api/venue/get', venueController.getVenuesByCategory);
  router.get('/api/venue/search', venueController.searchVenue);
  router.get('/api/venue/:venue_id', venueController.getVenueById);
  router.put('/api/venue/:venue_id', passport.loginRequired, authorizationModule.authorizeAny, venueController.updateVenue);
  router.post('/api/venue', passport.loginRequired, authorizationModule.authorizeAny, venueController.createVenue);
  router.post('/api/venue/upload/:venue_id', passport.loginRequired, authorizationModule.authorizeAny,
      upload.single('photo'), venueController.uploadFile);
  router.delete('/api/venue/:venue_id', passport.loginRequired, authorizationModule.authorizeAny, venueController.deleteVenue);
  router.post('/api/venue/rate', passport.loginRequired, venueController.rateVenue);
  router.get('/api/venue/rate/user/:venue_id', passport.loginRequired, venueController.getRateForUser);

  // Category routes
  router.get('/api/category', categoryController.getCategories);
  router.post('/api/category', passport.loginRequired, authorizationModule.authorizeAny, categoryController.createCategory);
  router.get('/api/category/get', categoryController.getCategoryByName);
  router.get('/api/category/:category_id', categoryController.getCategory);
  router.put('/api/category/:category_id', passport.loginRequired, authorizationModule.authorizeAny, categoryController.updateCategory);
  router.delete('/api/category/:category_id', passport.loginRequired, authorizationModule.authorizeAny, categoryController.deleteCategory);

  // Category Venue routes
  router.get('/api/category-venue', categoryVenueController.getCategoriesVenues);
  router.get('/api/category-venue/category/:category_id', categoryVenueController.getVenuesByCategoryId);
  router.get('/api/category-venue/venue/:venue_id', categoryVenueController.getCategoriesByVenueId);
  
  // Review routes
  router.get('/api/review', reviewController.getReviews);
  router.get('/api/review/venue/:venue_id', reviewController.getReviewsByVenueId);
  router.post('/api/review', passport.loginRequired, reviewController.createReview);
  router.delete('/api/review/:review_id',  passport.loginRequired, reviewController.deleteReview);
	
  // Language routes
  router.get('/api/language/change', languageController.changeLanguage);
  router.get('/api/language', languageController.getLanguages);
  router.post('/api/language', passport.loginRequired, authorizationModule.authorizeAdmin, languageController.createLanguage);
  router.get('/api/language/:language_id', languageController.getLanguage);
  router.put('/api/language/:language_id', passport.loginRequired, authorizationModule.authorizeAdmin, languageController.updateLanguage);
  router.delete('/api/language/:language_id', passport.loginRequired, authorizationModule.authorizeAdmin, languageController.deleteLanguage);

  // Authentication routes
  router.post('/api/auth/register', passport.logoutFirst, userController.register);
  router.post('/api/auth/login', passport.logoutFirst, userController.login);
  router.get('/api/auth/logout', passport.loginRequired, userController.logout);

  // User routes
  router.get('/api/user', passport.loginRequired, authorizationModule.authorizeAny, userController.getUsers);
  router.put('/api/user/:user_id', passport.loginRequired, authorizationModule.authorizeAdmin, userController.updateUser);
  router.delete('/api/user/:user_id', passport.loginRequired, authorizationModule.authorizeAdmin, userController.deleteUser);

  // Session test
  router.get('/api/session', (req, resp) => { resp.status(200).json(req.session); });

  router.post('/api/reset_password', resetPasswordController.sendResetMail);
  router.put('/api/reset_password/:token', resetPasswordController.resetPassword);

  router.get('*', staticController.serve404);

  router.authorizationModule = authorizationModule;
  return router;
};
