/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('venue_localized_data', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    venue_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'venue',
        key: 'id'
      }
    },
    language_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'language',
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    created_at: {
      type: DataTypes.TIME,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.TIME,
      allowNull: true
    }
  }, {
    tableName: 'venue_localized_data',
    underscored: true
  });
};
