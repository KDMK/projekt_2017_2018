/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('rating', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    venue_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'venue',
        key: 'id'
      }
    },
    rating: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    created_by: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    edited_by: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
          model: 'users',
          key: 'id'
      }
    },
    created_at: {
      type: DataTypes.TIME,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.TIME,
      allowNull: true
    }
  }, {
    tableName: 'rating',
    underscored: true
  });
};
