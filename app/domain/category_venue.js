/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('category_venue', {
        id: {
            type: DataTypes.BIGINT,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        category_id: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: 'category',
                key: 'id'
            }
        },
        venue_id: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: 'venue',
                key: 'id'
            }
        },
        created_at: {
            type: DataTypes.TIME,
            allowNull: true
        },
        updated_at: {
            type: DataTypes.TIME,
            allowNull: true
        }
    }, {
        tableName: 'category_venue',
        underscored: true
    });
};
