function ResponseData(success, message, data) {
  this.success = success;
  this.message = message;
  this.data = data;
}

function _handleResponse(res, code, statusMsg, data) {
  let success = code === 200;
  res.status(code).json(new ResponseData(success, statusMsg, data));
}

module.exports = {
  ResponseData: ResponseData,
  handleResponse: _handleResponse
};