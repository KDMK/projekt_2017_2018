/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('language', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  }, {
    tableName: 'language',
    underscored: true
  });
};
