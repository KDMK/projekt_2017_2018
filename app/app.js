( function () {

  const ASSETS = '/assets';
  const WIKI = '/wiki';
  const TEST = 'test';
  const DEVELOPMENT = 'dev';
  const PRODUCTION = 'prod';
  const TEST_LOCATION = 'app/environment/test.env';
  const DEVELOPMENT_LOCATION = 'app/environment/dev.env';
  const PRODUCTION_LOCATION = 'app/environment/prod.env';
  if (process.env.NODE_ENV === TEST) {

    require('dotenv').config({path: TEST_LOCATION});
  } else if (process.env.NODE_ENV === DEVELOPMENT) {
    require('dotenv').config({path: DEVELOPMENT_LOCATION});
  } else if (process.env.NODE_ENV === PRODUCTION) {
      require('dotenv').config({path: PRODUCTION_LOCATION});
  }
  const UPLOADS = process.env.HOME + '/temp/uploads/';

// IMPORTS ----------------------------------------------------------------------------------------
  const express = require('express');
  const bodyParser = require('body-parser');
  const passport = require('passport');
  const session = require('express-session');
  const flash = require('connect-flash');
  const cookieParser = require('cookie-parser');
  const multer = require('multer');
  const STORAGE_ROOT = process.env.HOME + '/projectUploads/venue'

  const upload = multer();
  const app = express();
  const dao = require('./persistence/DAO')();
  const logger = require('morgan');

  console.log(`Running in ${process.env.NODE_ENV}`);
  console.log(__dirname.replace('/app', ''));

// CONFIGURATIONS ---------------------------------------------------------------------------------
  const port = process.env.PORT || 8080;

  app.use(bodyParser.urlencoded({extended: true}));
  app.use(bodyParser.json());
  app.use(cookieParser());
  app.use(logger('combined'));

// Passport and user session
  // TODO: Implement connect-session-sequelize later. By default this uses in-memory storage for sessions and it's not appropriate for production).
  app.use(session({
    secret: process.env.SECRET_KEY,
    resave: false,
    saveUninitialized: true
  }));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(flash());
  require('./core/authenticationModule')(passport, dao);

  const router = require('./core/routerModule')(dao, passport, upload);
  app.use('/', express.static(__dirname + ASSETS));
  app.use('/res/venue/image', express.static(STORAGE_ROOT));
  app.use('/uploads/', express.static(UPLOADS));
  app.use('/wiki', passport.loginRequired);
  app.use('/wiki', router.authorizationModule.authorizeAdmin);
  app.use('/wiki', express.static(__dirname.replace('/app', '') + WIKI));
  app.use('/', router);

// START SERVER -----------------------------------------------------------------------------------
  app.listen(port);
  console.log(`Server started on port ${port}.`);

  module.exports = app;
}());
